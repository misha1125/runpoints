package com.highscores.runstory.Utility;

import android.util.Base64;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.jjoe64.graphview.series.DataPoint;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by михаил on 26.06.2017.
 */

public class StringCoder {

    public static String latLngToString(List<LatLng> positions){
        String rez = "";
        for (LatLng ln : positions) {
            rez += ln.latitude + " " + ln.longitude + " ";
        }

        return rez;
    }

    public static List<LatLng> stringToLatLng(String base){
        Log.d("LatLng",base);
        List<LatLng> rez = new ArrayList<>();
        String[]points = base.split(" ");
        if(points.length<=1)return rez;
        for(int i =0;i<points.length;++i){
            Log.d("LatLng",points[i] + " " + points[i+1]);

            rez.add(new LatLng(Double.parseDouble(points[i]),Double.parseDouble(points[i+1])));
            ++i;
        }
        return rez;
    }

    public static String dataPointToString(List<DataPoint> points){
        String rez = "";
        for (DataPoint ln : points) {
            rez += ln.getX() + " " + ln.getY() + " ";
        }

        return rez;
    }

    public static List<DataPoint> stringToDataPoint(String base){
        Log.d("strngt","dp " + base);
        Scanner in = new Scanner(base);
        List<DataPoint> rez = new ArrayList<>();
        String[]points = base.split(" ");
        if(points.length<=1)return rez;
        for(int i =0;i<points.length;++i){
            Log.d("LatLng",points[i] + " " + points[i+1]);

            rez.add(new DataPoint(Double.parseDouble(points[i]),Double.parseDouble(points[i+1])));
            ++i;
        }
        return rez;
    }

}
