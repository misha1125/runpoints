package com.highscores.runstory.Utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.highscores.runstory.Model.Training.Training;
import com.highscores.runstory.data.TrainingBD;
import com.highscores.runstory.data.TrainingContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by михаил on 26.06.2017.
 */

public class TrainingSaver {
    List<Training> trainings;
    private TrainingBD mDbHelper;

    Context mContext;

    static TrainingSaver instance;

    private TrainingSaver(Context context){
        mContext = context;
        mDbHelper = new TrainingBD(mContext);
    }

    public static TrainingSaver instance(Context context){
        if(instance == null){
            instance = new TrainingSaver(context);
        }
        return instance;
    }

    public void saveTraining(Training training){
        // Считываем данные из текстовых полей
        int time = training.gameParameters.getTime();
        double distance = training.gameParameters.getDistance();
        int calories = (int)training.gameParameters.getCalories();
        String positions = StringCoder.latLngToString(training.gameParameters.getPoints());
        String speed = StringCoder.dataPointToString(training.gameParameters.speedsList);

        long date = training.date;



        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TrainingContract.TrainingEntry.COLUMN_TIME, time);
        values.put(TrainingContract.TrainingEntry.COLUMN_DISTANCE, distance);
        values.put(TrainingContract.TrainingEntry.COLUMN_CALORIES, calories);
        values.put(TrainingContract.TrainingEntry.COLUMN_DATE, date);
        values.put(TrainingContract.TrainingEntry.COLUMN_SPEED, speed);
        values.put(TrainingContract.TrainingEntry.COLUMN_POINTS, positions);
        values.put(TrainingContract.TrainingEntry.COLUMN_MAX_SPEED, training.gameParameters.getMaxSpeed());
        values.put(TrainingContract.TrainingEntry.COLUMN_MONEYS, training.gameParameters.getCoins());

        // Вставляем новый ряд в базу данных и запоминаем его идентификатор
        long newRowId = db.insert(TrainingContract.TrainingEntry.TABLE_NAME, null, values);

        // Выводим сообщение в успешном случае или при ошибке
        if (newRowId == -1) {
            // Если ID  -1, значит произошла ошибка
            Toast.makeText(mContext, "Ошибка при сохранении", Toast.LENGTH_SHORT).show();
        } else {

        }
    }

    public List<Training> getTrainings(){
        displayDatabaseInfo();
        return trainings;
    }



    private void displayDatabaseInfo() {
        // Создадим и откроем для чтения базу данных
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Зададим условие для выборки - список столбцов
        String[] projection = {
                TrainingContract.TrainingEntry._ID,
                TrainingContract.TrainingEntry.COLUMN_DISTANCE,
                TrainingContract.TrainingEntry.COLUMN_CALORIES,
                TrainingContract.TrainingEntry.COLUMN_DATE,
                TrainingContract.TrainingEntry.COLUMN_SPEED,
                TrainingContract.TrainingEntry.COLUMN_POINTS,
                TrainingContract.TrainingEntry.COLUMN_TIME,
                TrainingContract.TrainingEntry.COLUMN_MAX_SPEED,
                TrainingContract.TrainingEntry.COLUMN_MONEYS,};

        // Делаем запрос
        Cursor cursor = db.query(
                TrainingContract.TrainingEntry.TABLE_NAME,   // таблица
                projection,            // столбцы
                null,                  // столбцы для условия WHERE
                null,                  // значения для условия WHERE
                null,                  // Don't group the rows
                null,                  // Don't filter by row groups
                null);                   // порядок сортировки

        try {
            // Узнаем индекс каждого столбца
            int idColumnIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry._ID);
            int distanceIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_DISTANCE);
            int caloriesIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_CALORIES);
            int dateIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_DATE);
            int speedIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_SPEED);
            int pointsIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_POINTS);
            int timeIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_TIME);
            int maxSpeedIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_MAX_SPEED);
            int moneyIndex = cursor.getColumnIndex(TrainingContract.TrainingEntry.COLUMN_MONEYS);
            trainings = new ArrayList<>();

            // Проходим через все ряды
            while (cursor.moveToNext()) {
                // Используем индекс для получения строки или числа
                int currentID = cursor.getInt(idColumnIndex);
                double currentDistance = cursor.getDouble(distanceIndex);
                int currentCalories = cursor.getInt(caloriesIndex);
                long currentDate = cursor.getLong(dateIndex);
                String currentSpeed = cursor.getString(speedIndex);
                String currentPoints = cursor.getString(pointsIndex);
                int currentTime = cursor.getInt(timeIndex);
                double currentMaxSpeed = cursor.getDouble(maxSpeedIndex);
                int currentMoneys = cursor.getInt(moneyIndex);

                Training training = new Training();
                training.gameParameters.setDistance(currentDistance);
                training.gameParameters.setTime(currentTime);
                training.gameParameters.setCalories(currentCalories);
                training.date = currentDate;
                training.gameParameters.setPoints(StringCoder.stringToLatLng(currentPoints));
                training.gameParameters.speedsList = StringCoder.stringToDataPoint(currentSpeed);
                training.gameParameters.setMaxSpeed((float) currentMaxSpeed);
                training.gameParameters.setCoins(currentMoneys);

                trainings.add(training);
            }
        } finally {
            cursor.close();
        }
    }
}
