package com.highscores.runstory.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by михаил on 14.03.2017.
 */

public class Constance {
    public static final double MAX_MONEY_CREATING_RADIUS = 600;
    public static final double MIN_POINT_CREATING_RADIUS = 350;
    public static final int MONEYS_PER_KM = 5;
    public static final String DIRECTIONS_API_KEY = "AIzaSyBezx4aVn13dOMDGdA6fmtX1usFFNILL-o";
    private static final double R =6371000;

    public static double distance (LatLng gp1, LatLng gp2 )
    {
        int radius = 6371;
        double dLat = Math.toRadians(gp2.latitude - gp1.latitude);
        double dLong = Math.toRadians(gp2.longitude - gp1.longitude);
        double lat1 = Math.toRadians(gp1.latitude);
        double lat2 = Math.toRadians(gp2.latitude);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return c * R;
    }

    public static LatLng randomLatLng(LatLng current,double bigRadius,double smallRadius){
        Random rand = new Random(System.currentTimeMillis());
        double distance =rand.nextDouble()*(bigRadius-smallRadius)+smallRadius;
        double bearing = rand.nextDouble()*Math.PI*2;
        double x = distance*Math.sin(bearing);
        double y = distance*Math.cos(bearing);
        return new LatLng(metersToLat(x)+current.latitude,current.longitude+metersToLng(current.longitude,y));
    }

    public static LatLng randomLatLng(LatLng current,double radius){
        Random rand = new Random(System.currentTimeMillis());
        double distance = radius;
        double bearing = rand.nextDouble()*Math.PI*2;
        double x = distance*Math.sin(bearing);
        double y = distance*Math.cos(bearing);
        return new LatLng(metersToLat(x)+current.latitude,current.longitude+metersToLng(current.longitude,y));
    }
    static final double LAT_LEN =  20004274/180;
    static final double EQUATOR_LEN = 40075696/360;
    private static double metersToLat(double val){
        return val/LAT_LEN;
    }

    private static double metersToLng(double currentAngel,double val){
        return val/(EQUATOR_LEN * Math.cos(currentAngel*Math.PI/180));
    }


    public static BitmapDescriptor bitmapScale(Context context, int rez, float size){
        Bitmap b = BitmapFactory.decodeResource(context.getResources(),rez);
        float sizeSm = (float) b.getWidth() / (float) b.getDensity();
        float rezSize = 0.1f * (float) size;
        float k = rezSize/sizeSm;
        Log.d("Sizes",b.getWidth()*k + " " + b.getHeight()*k);
        b = Bitmap.createScaledBitmap(b,(int)(b.getWidth()*k),
                (int)(b.getHeight()*k), true);
        return BitmapDescriptorFactory.fromBitmap(b);
    }
    public static BitmapDescriptor smallBitmap(Context context,int rez){
        return bitmapScale(context,rez,0.7f);
    }

    public static String formatLatLng(LatLng l){
        return l.latitude+","+l.longitude;
    }

    public static String formatWaypoints(List<Marker> points){
        final String VIA = "via:";
        if(points.size() == 0 || points.size() == 1){
            return "";
        }
        if(points.size() == 2){
            return VIA + formatLatLng(points.get(0).getPosition());
        }
        String res = VIA + formatLatLng(points.get(0).getPosition());
        for(int i = 1;i<points.size()-1;++i){
            res += "|" +  VIA + formatLatLng(points.get(i).getPosition());
        }
        return res;
    }

    public static ArrayList<LatLng> decodePolyPoints(String encodedPath){
        int len = encodedPath.length();

        final ArrayList<LatLng> path = new ArrayList<LatLng>();
        int index = 0;
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int result = 1;
            int shift = 0;
            int b;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lat += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            result = 1;
            shift = 0;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while (b >= 0x1f);
            lng += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

            path.add(new LatLng(lat * 1e-5, lng * 1e-5));
        }

        return path;
    }
}
