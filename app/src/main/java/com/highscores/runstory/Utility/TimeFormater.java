package com.highscores.runstory.Utility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by михаил on 26.06.2017.
 */

public class TimeFormater {

    public static String formatDate(long time){
        Date date = new Date(time);
        return new  SimpleDateFormat("dd MMMM").format(date);
    }

    public static String formatTime(int time){
     String timeOutput = "";
      int sec = time%60;
      time/=60;
      int minuts = time % 60;
      time/=60;
      if(time != 0){
         timeOutput = time + ":";
      }
      if(minuts<10){
         timeOutput += "0";
      }
      timeOutput+=minuts + ":";
      if(sec<10){
         timeOutput+="0";
      }
      timeOutput +=sec;
      return timeOutput;
    }
}
