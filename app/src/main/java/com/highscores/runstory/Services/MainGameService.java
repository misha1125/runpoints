package com.highscores.runstory.Services;


import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.highscores.runstory.Activities.Game.RunningActivity;
import com.highscores.runstory.R;

import static com.highscores.runstory.Services.GameDisplay.*;


public class MainGameService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private NotificationManager mNotificationManager;

    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;

    private int mTime;
    private double mDistance;
    private float mSpeed;

    private LocationRequest mLocationRequest;

    private Intent mActivityIntent;
    private double mLat, mLng;

    boolean mIsRunning = true;

    boolean mIsWorkService;
    long beginTime;

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        onHandleIntent(intent);
        return START_STICKY;
    }

    protected void onHandleIntent(@Nullable Intent intent) {
        beginTime = System.currentTimeMillis();
        Log.d("MyTag", "Launch");
        runTimer();
        if (intent != null) {
            mDistance = intent.getDoubleExtra(DISTANCE, 0);
            mTime = intent.getIntExtra(TIME, 0);
            mIsWorkService = intent.getBooleanExtra(RunningActivity.WORK_SERVICE, false);
        }
        if (mIsWorkService) {
            ShowNotification();
        }
        mActivityIntent = new Intent(GameDisplay.GAME_SERVICE_INTENT);
        try {
            checkApiClient();
            mGoogleApiClient.connect();
        } catch (Exception e) {
            Log.e("MyTag", "Failed to connect");
        }

    }


    private void ShowNotification() {
        Notification.Builder builder = new Notification.Builder(getApplicationContext());

        builder.setContentText("Тренеровка запущена");
        builder.setContentTitle("RunPoints");
        builder.setSmallIcon(R.drawable.aplication_logo_not);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.aplication_logo_not));
        builder.setTicker("Тренировка запущена");
        builder.setWhen(System.currentTimeMillis());


      //  Intent actionIntent = new Intent(getApplicationContext(), RunningActivity.class);
       // actionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
       // PendingIntent actionPending = PendingIntent.getActivity(getApplicationContext(), 0, actionIntent, PendingIntent.FLAG_CANCEL_CURRENT);
      //  builder.setContentIntent(actionPending);

        Notification notification = builder.getNotification();
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        mNotificationManager.notify(123, notification);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MyTag", "Destroy");
        mGoogleApiClient.disconnect();
        mNotificationManager.cancel(123);
        mIsRunning = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void checkApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d("MyTag", "Start");
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    int i = 0;

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (mLastLocation != null) {
                mDistance += location.distanceTo(mLastLocation) / 1000;
                Log.d("MyTag", "Up");

            }
            mSpeed = location.getSpeed() * 3.6f;
            mLat = location.getLatitude();
            mLng = location.getLongitude();
            mLastLocation = location;
            postInformationToActivity();
        }
    }

    protected void startLocationUpdates() {
        if (mLocationRequest == null) {
            createLocationRequest();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void runTimer() {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(!mIsRunning)return;
                Log.d("MyTag","Timer " + ((System.currentTimeMillis() - beginTime)/1000 + mTime));
                postTime();
                handler.postDelayed(this, 1000);
            }
        });
    }

    private void postInformationToActivity(){
        mActivityIntent.putExtra(DISTANCE,mDistance);
        mActivityIntent.putExtra(SPEED,mSpeed);
        mActivityIntent.putExtra(LAT,mLat);
        mActivityIntent.putExtra(LNG,mLng);
        mActivityIntent.putExtra(RunningActivity.ONLY_TIME,false);
        sendBroadcast(mActivityIntent);
    }

    private void postTime(){
        mActivityIntent.putExtra(TIME,(int)((System.currentTimeMillis() - beginTime)/1000 + mTime));
        mActivityIntent.putExtra(RunningActivity.ONLY_TIME,true);
        sendBroadcast(mActivityIntent);
    }


}
