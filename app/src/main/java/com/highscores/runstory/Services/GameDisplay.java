package com.highscores.runstory.Services;

import android.location.Location;

/**
 * Created by михаил on 03.04.2017.
 */

public interface GameDisplay {
    String GAME_SERVICE_INTENT = "GSI";
    String DISTANCE = "DIST";
    String SPEED = "SPEED";
    String LOCATIONS = "LOC";
    String TIME = "TIME";
    String LAT = "lat";
    String LNG = "lng";
}
