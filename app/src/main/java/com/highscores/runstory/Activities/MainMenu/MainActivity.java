package com.highscores.runstory.Activities.MainMenu;

import android.Manifest;
import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.app.Fragment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.highscores.runstory.Activities.Hello.HelloActivity;
import com.highscores.runstory.Activities.MainMenu.Fragments.Achievements.AchievementsFragment;
import com.highscores.runstory.Activities.MainMenu.Fragments.MainFragments.MainFragment;
import com.highscores.runstory.Activities.MainMenu.Fragments.MyPage.MyPageFragment;
import com.highscores.runstory.Activities.MainMenu.Fragments.Schedule.ScheduleFragment;
import com.highscores.runstory.Activities.MainMenu.Fragments.Shop.ShopFragment;
import com.highscores.runstory.Activities.MainMenu.Fragments.Statistic.StatisticFragment;
import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.R;
import com.highscores.runstory.Receiver.AlarmReceiver;
import com.highscores.runstory.Utility.Constance;
import com.highscores.runstory.Utility.TimeFormater;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 10;
    LinearLayout mFragmentsContainer;

    Fragment mCurrentFragment;

    int lastFragment = 0;

    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if(!Player.instance(this).isFirstTime()){
            View navHeaderView= navigationView.getHeaderView(0);
            TextView helloUserView = (TextView) navHeaderView.findViewById(R.id.hello_user_herder);
            helloUserView.setText(getString(R.string.hello_user) +" " +  Player.instance(this).getName());
            TextView level = (TextView) navHeaderView.findViewById(R.id.level_nav_view);
            level.setText(String.valueOf(Player.instance(this).getLevel()));
        }

        checkPermission();

        mFragmentsContainer = (LinearLayout) findViewById(R.id.fragment_container_main);
        if(savedInstanceState==null){
            standMainFragment();
        }else{
            mFragmentsContainer.removeAllViews();
            lastFragment = savedInstanceState.getInt("last");
            Log.d("lastFR",lastFragment+"");
        }
        checkFirstTime();
        AlarmReceiver.repeat(this);
        applyAdd();
    }

    void applyAdd(){
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("last",lastFragment);
        Log.d("lastFR",lastFragment+"s");
    }

    private void checkFirstTime(){
        if(Player.instance(this).isFirstTime()){
            Intent intent = new Intent(this, HelloActivity.class);
            startActivity(intent);
        }
    }


    private void changeParameters(){
        Player.instance(this).increaseCoinsCount(10000);
        Player.instance(this).increaseCalories(1100);
        Player.instance(this).increaseDistance(20);
        Player.instance(this).checkMaxSpeed(20);
    }


    private void checkPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                Toast.makeText(this,"Для работы этого приложения необходимо дать согласие на обработку вашего местоположения",Toast.LENGTH_SHORT).show();

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        &&grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(this,"Разрешение не получено, определение местоположения недоступно",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onStart(){
        super.onStart();

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button_green, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.


        int id = item.getItemId();
        if(mCurrentFragment == null){
            mCurrentFragment = new MainFragment();
        }
        switch (id) {
            case R.id.nav_main:
                if (lastFragment != 0) {
                    standMainFragment();
                    lastFragment = 0;
                }
                break;
            case R.id.nav_profile:
                if (lastFragment != 1) {
                    standProfileFragment();
                    lastFragment = 1;
                }
                break;
            case R.id.nav_achievements:
                if (lastFragment != 2) {
                    standFragmentWithText(new AchievementsFragment(), getString(R.string.achievements));
                    lastFragment = 2;
                }
                break;
            case R.id.nav_shop:
                if (lastFragment != 3) {
                    standFragmentWithText(new ShopFragment(), getString(R.string.shop));
                    lastFragment = 3;
                }
                break;
            case R.id.nav_schedule:
                if (lastFragment != 4) {
                    standFragmentWithText(new ScheduleFragment(), getString(R.string.schedule));
                    lastFragment = 4;
                }
                break;
            case R.id.nav_statistic:
                if (lastFragment != 5) {
                    standFragmentWithText(new StatisticFragment(), getString(R.string.statistic));
                    lastFragment = 5;
                }
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private void standMainFragment(){
        standFragmentWithText(new MainFragment(),getString(R.string.name_activity_running));

    }
    private void standProfileFragment(){
        standFragmentWithText(new MyPageFragment(),getString(R.string.my_page));
    }

    private void standFragmentWithText(Fragment fragment,String text){
        setTitle(text);
        standFragment(fragment);
    }

    private void standFragment(Fragment fragment){
        mFragmentsContainer.removeAllViews();
        FragmentTransaction trans = getFragmentManager().beginTransaction();
        if(trans.isEmpty()){
            trans.add(R.id.fragment_container_main, fragment);
        }else {
            trans.replace(R.id.fragment_container_main, fragment);
        }
        trans.commit();
    }

    public void goToShop(){
        if (lastFragment != 3) {
            standFragmentWithText(new ShopFragment(), getString(R.string.shop));
            lastFragment = 3;
        }
    }
}
