package com.highscores.runstory.Activities.MainMenu.Fragments.MainFragments;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.highscores.runstory.Activities.Game.CreateTrainingActivity;
import com.highscores.runstory.Activities.Game.RunningActivity;
import com.highscores.runstory.Adapters.TasksAdapter;
import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.Model.Task.TasksList;
import com.highscores.runstory.R;


public class MainFragment extends Fragment {


    RecyclerView mTasksRV;
    Button mFreeTraning;
    View mHerder;
    LinearLayout mMainContainer;

    public MainFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mTasksRV = (RecyclerView) rootView.findViewById(R.id.tasks_recycler_view);
        mFreeTraning = (Button) rootView.findViewById(R.id.launch_free_traning);

        mHerder =  rootView.findViewById(R.id.player_parameters);
        mMainContainer = (LinearLayout) rootView.findViewById(R.id.container_main);

        ((TextView)rootView.findViewById(R.id.total_distance_main)).setText(String.format("%.2f",Player.instance(getActivity()).getDistance()) + " " +  getString(R.string.km));
        ((TextView)rootView.findViewById(R.id.high_score_main)).setText(String.format("%.2f",Player.instance(getActivity()).getMaxDistance()) + " " +  getString(R.string.km));
        mFreeTraning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateTrainingActivity.class);
                startActivity(intent);
            }
        });
        ApplyPlayerView();
        ApplyTasksListView();
        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();

    }

    private void ApplyPlayerView(){

    }

    private void ApplyTasksListView(){
        //TODO: убрать этот костыль
        mMainContainer.removeView(mHerder);
        TasksAdapter tasksAdapter = new TasksAdapter(TasksList.getLastTacks(getActivity().getApplicationContext()),mHerder,getActivity());
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mTasksRV.setLayoutManager(layoutManager);
        mTasksRV.setAdapter(tasksAdapter);
    }


}
