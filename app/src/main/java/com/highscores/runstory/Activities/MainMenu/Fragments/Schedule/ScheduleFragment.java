package com.highscores.runstory.Activities.MainMenu.Fragments.Schedule;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.highscores.runstory.R;
import com.highscores.runstory.Receiver.AlarmReceiver;
import com.highscores.runstory.Utility.TimeFormater;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends Fragment implements RadioButton.OnCheckedChangeListener {


    ToggleButton mn;
    ToggleButton ts;
    ToggleButton wn;
    ToggleButton tr;
    ToggleButton fr;
    ToggleButton st;
    ToggleButton sn;

    public static final  String WEEK = "week";
    public static final String MIN= "min";
    public static final String HOUR = "hour";

    Button mSaveButton;

    TimePicker mTimePicker;

    boolean [] mWeek = new boolean[7];
    long mTime;

    static final String APP_PREFERENCES = "schedule";

    private SharedPreferences mSettings;

    boolean isSaved = true;

    public ScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_schedule, container, false);

        mTimePicker = (TimePicker) root.findViewById(R.id.scheduleTime);
        mTimePicker.setIs24HourView(true);

        mn = (ToggleButton) root.findViewById(R.id.monday);
        ts = (ToggleButton) root.findViewById(R.id.tuesday);
        wn = (ToggleButton) root.findViewById(R.id.wednesday);
        tr = (ToggleButton) root.findViewById(R.id.thursday);
        fr = (ToggleButton) root.findViewById(R.id.friday);
        st = (ToggleButton) root.findViewById(R.id.saturday);
        sn = (ToggleButton) root.findViewById(R.id.sunday);

        mSaveButton = (Button) root.findViewById(R.id.save_shedule);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyNotifications();
               hideSaveButton();
            }
        });

        read();
        mn.setOnCheckedChangeListener(this);
        ts.setOnCheckedChangeListener(this);
        wn.setOnCheckedChangeListener(this);
        tr.setOnCheckedChangeListener(this);
        fr.setOnCheckedChangeListener(this);
        st.setOnCheckedChangeListener(this);
        sn.setOnCheckedChangeListener(this);



        mTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                showSaveButton();
            }
        });
        return root;
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        checkChecked((ToggleButton) buttonView);

        switch (buttonView.getId()){
            case R.id.monday:
                mWeek[0] = isChecked;
                break;
            case R.id.tuesday:
                mWeek[1] = isChecked;
                break;
            case R.id.wednesday:
                mWeek[2] = isChecked;
                break;
            case R.id.thursday:
                mWeek[3] = isChecked;
                break;
            case R.id.friday:
                mWeek[4] = isChecked;
                break;
            case R.id.saturday:
                mWeek[5] = isChecked;
                break;
            case R.id.sunday:
                mWeek[6] = isChecked;
                break;
        }
       showSaveButton();

    }


    void applyNotifications(){
        showNotification();
        save();
    }

    private void showNotification(){
        AlarmManager alarmManager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(getActivity(), AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY,mTimePicker.getCurrentHour()+1);
        calendar.set(Calendar.MINUTE,mTimePicker.getCurrentMinute());

        long timeToStart = calendar.getTimeInMillis();
        if(System.currentTimeMillis()>timeToStart){
            timeToStart+= 24 * 60 *60 * 1000;
        }
        Log.d("timeStart", TimeFormater.formatDate(timeToStart) + TimeFormater.formatTime((int)((timeToStart%(24 * 60 * 60 * 1000))/1000)) + "shudle");
        alarmManager.set(AlarmManager.RTC_WAKEUP, timeToStart, alarmIntent);
    }



    public static int dayOfWeek(int i){
        if(i == 0)return Calendar.MONDAY;
        if(i == 1) return Calendar.TUESDAY;
        if(i == 2) return Calendar.WEDNESDAY;
        if(i == 3) return Calendar.THURSDAY;
        if(i == 4) return Calendar.FRIDAY;
        if(i == 5) return Calendar.SATURDAY;
        if(i == 6) return Calendar.SUNDAY;
        return 0;
    }

    public static int indexFromDay(int day){
        if(day == Calendar.MONDAY) return 0;
        if(day == Calendar.TUESDAY) return 1;
        if(day == Calendar.WEDNESDAY) return 2;
        if(day == Calendar.THURSDAY) return 3;
        if(day == Calendar.FRIDAY) return 4;
        if(day == Calendar.SATURDAY) return 5;
        if(day == Calendar.SUNDAY) return 6;
        return -1;
    }

    void read(){
        mSettings =  getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        mTimePicker.setCurrentMinute(mSettings.getInt(MIN,0));
        mTimePicker.setCurrentHour(mSettings.getInt(HOUR,13)-1);
        mWeek = readWeek(getActivity());
        applyButtonsFromWeekArray();
    }

    public static boolean [] readWeek(Context context){
        SharedPreferences settings =  context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        boolean[] week = new boolean[7];
        for(int i =0;i<7;++i){
            week[i] = settings.getBoolean(WEEK + i, true);
        }
        return week;
    }


    private void showSaveButton(){
        if(isSaved){
            isSaved = false;
            Animation bottomUp = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.button_up);
            mSaveButton.startAnimation(bottomUp);
            mSaveButton.setVisibility(View.VISIBLE);
        }

    }

    private void hideSaveButton(){
        if(!isSaved){
            isSaved = true;
            Animation bottomDown = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.button_down);
            mSaveButton.startAnimation(bottomDown);
            mSaveButton.setVisibility(View.INVISIBLE);
        }

    }

    //<summary>
    //0 - min,1 - hour
    //</summary>
    public static int [] readTime(Context context){
        SharedPreferences settings =  context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        int [] rez = new int[2];
        rez[0] = settings.getInt(MIN,0);
        rez[1] = settings.getInt(HOUR,12);
        return rez;
    }


    void save(){
        mSettings =  getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        for(int i =0;i<7;++i){
            editor.putBoolean(WEEK + i, mWeek[i]);
        }
        editor.putInt(HOUR,mTimePicker.getCurrentHour() + 1);
        editor.putInt(MIN,mTimePicker.getCurrentMinute());
        editor.apply();
    }

    void applyButtonsFromWeekArray(){
        mn.setChecked(mWeek[0]);
        checkChecked(mn);

        ts.setChecked(mWeek[1]);
        checkChecked(ts);

        wn.setChecked(mWeek[2]);
        checkChecked(wn);

        tr.setChecked(mWeek[3]);
        checkChecked(tr);

        fr.setChecked(mWeek[4]);
        checkChecked(fr);

        st.setChecked(mWeek[5]);
        checkChecked(st);

        sn.setChecked(mWeek[6]);
        checkChecked(sn);
    }

    void checkChecked(ToggleButton btn){
        if(btn.isChecked()){
            btn.setTextColor(getResources().getColor(R.color.white));
        }else {
            btn.setTextColor(getResources().getColor(R.color.primaryText));
        }
    }
}
