package com.highscores.runstory.Activities.Game;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highscores.runstory.API.Response.RouteResponse;
import com.highscores.runstory.API.RouteApi;
import com.highscores.runstory.Controller.GameController;
import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Model.Task.RunTimeTask;
import com.highscores.runstory.Model.Task.RunToPointTask;
import com.highscores.runstory.Utility.Constance;
import com.highscores.runstory.Model.MapPoint.MoneyPoint;
import com.highscores.runstory.Model.Task.Task;
import com.highscores.runstory.Model.Task.TasksList;
import com.highscores.runstory.R;
import com.highscores.runstory.Utility.TimeFormater;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CreateTrainingActivity extends FragmentActivity implements OnMapReadyCallback,GameView {

    private GameController mGameController;
    private GoogleMap mMap;
    Marker locationMarker;

    Button mHelpButton;
    Button mBeginTrainingButton;
    TextView mDistanceTV;

    ArrayList<Marker> mMarkers = new ArrayList<>();
    int mMarkersCount;

    TextView taskTV;
    TextView profitTV;
    View taskIconView;


    LatLng mCurrentPosition;
    private Retrofit mRetrofit;
    private RouteApi mApi;
    public static final String DIRECTIONS_BASE_LINK = "https://maps.googleapis.com";
    final int MAX_MARKER_COUNT = 8;

    List<LatLng> mDecodedPoints;
    Task mTask;
    PolylineOptions wayLineOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_training);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.creating_map);
        mapFragment.getMapAsync(this);

        Log.d("testttt", TimeFormater.formatDate(System.currentTimeMillis()));
        mHelpButton = (Button) findViewById(R.id.add_marker);
        mBeginTrainingButton = (Button) findViewById(R.id.start_tranning_activity);
        mDistanceTV = (TextView) findViewById(R.id.distance_want);
        taskTV = (TextView) findViewById(R.id.task_value_plan);
        profitTV = (TextView) findViewById(R.id.task_cost_plan);
        taskIconView = findViewById(R.id.task_icon_plan);

        applyListeners();
        mGameController = GameController.instance(this,this);
        findTask();
    }

    private void applyListeners(){
        mHelpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               printHelp();

            }
        });
        mBeginTrainingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(CreateTrainingActivity.this,RunningActivity.class);
            startActivity(intent);
            }
        });
    }

    private void AddMarker(LatLng where) {
        if(mCurrentPosition!=null){
            if(mMarkers.size()<MAX_MARKER_COUNT){
                Marker m = mMap.addMarker(new MarkerOptions()
                        .position(where)
                        .draggable(true)
                      //  .icon(Constance.bitmapScale(CreateTrainingActivity.this, R.drawable.create_training,3))
                        .title(getString(R.string.how_to_move)));
                mMarkers.add(m);
                if(mMarkers.size()>0){
                    getWayPoliline();
                }
            }else {
                Toast.makeText(CreateTrainingActivity.this,getString(R.string.to_match_markers),Toast.LENGTH_SHORT).show();
            }

        }else {
            Toast.makeText(CreateTrainingActivity.this,getString(R.string.we_did_not_find),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        mGameController.shoutdown();
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.zoomTo(17));
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                AddMarker(latLng);
            }
        });
        createOnDragListener();
        wayLineOptions = new PolylineOptions().geodesic(true);

        wayLineOptions.color(getResources().getColor(R.color.wayPolyline));

        if(mTask!=null){
            mTask.getTaskController().changeMap(mMap);
        }

    }

    private void printHelp(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setTitle(getString(R.string.help));
        dlgAlert.setMessage(getString(R.string.help_text));
        dlgAlert.setPositiveButton("Понятно", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    private void findTask() {
        int num = getIntent().getIntExtra(Task.TASK_NUM, -1);
        if (num != -1) {
            mTask = TasksList.getTask(num, this);
            applyTaskUI();
        }
    }

    private void checkTask() {
        if(mTask!=null){

            mGameController.setTask(mTask);
            mTask.getTaskController().init(mCurrentPosition);
        }
    }

    void applyTaskUI(){
        if(mTask!=null){
            profitTV.setText(String.valueOf(mTask.getCost()));
            if(mTask instanceof RunToPointTask){
                taskIconView.setBackgroundResource(R.drawable.distance_previev);
                taskTV.setText(String.format("%.2f",(double)mTask.getCurrentValue()/1000) + " " + getString(R.string.km));
            }else if(mTask instanceof RunTimeTask){
                taskIconView.setBackgroundResource(R.drawable.time_icon);
                taskTV.setText(TimeFormater.formatTime(mTask.getCurrentValue()));
            }
        }
    }



    private void createOnDragListener() {
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                if(mMarkers.size()>0){
                    getWayPoliline();
                }
            }
        });
    }



    @Override
    public void updateUI(GameParameters gameParameters) {

    }

    @Override
    public void updateMap(boolean isRunning, List<LatLng> points, LatLng position) {
        updateLocationLabel(position);
        mCurrentPosition = position;
    }

    @Override
    public void onGetPositionFirstTime(LatLng position) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        mGameController.createMoneyPoints();
        mCurrentPosition = position;
        checkTask();

    }

    @Override
    public void standMoneyPoint(MoneyPoint point) {
        point.stand(mMap);
    }

    @Override
    public void putStartLabel(LatLng position) {

    }

    @Override
    public void putStopLabel(LatLng position) {

    }

    @Override
    public void showPowerButtons() {

    }

    @Override
    public void hidePowerButtons() {

    }

    @Override
    public void printTask(String test) {

    }

    @Override
    public void printCongratulations() {

    }


    public void putLocationLabel(LatLng position){
        locationMarker = mMap.addMarker(new MarkerOptions()
                .icon(Constance.smallBitmap(this,R.drawable.current_position))
                .anchor(0.5f,0.5f)
                .flat(true)
                .position(position));

    }

    public void updateLocationLabel(LatLng position) {
        if(locationMarker!=null){
            locationMarker.remove();
            Log.d("MapTag","invisible");
        }else {
            Log.d("MapTag","visible");
        }
        putLocationLabel(position);
    }

    private void getWayPoliline(){
        mRetrofit = new Retrofit.Builder()
                .baseUrl(DIRECTIONS_BASE_LINK)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mApi = mRetrofit.create(RouteApi.class);
        mApi.getRoute(Constance.formatLatLng(mCurrentPosition),
                Constance.formatLatLng(mMarkers.get(mMarkers.size()-1).getPosition()),
                true,getResources().getConfiguration().locale.getLanguage().equals("ru_RU")?"en":"ru","walking",Constance.formatWaypoints(mMarkers)
                ).enqueue(new Callback<RouteResponse>() {

            @Override
            public void onResponse(Call<RouteResponse> call, Response<RouteResponse> response) {
                drawWayPolyline(response.body());
                Log.d("MyTag3","ok");
            }

            @Override
            public void onFailure(Call<RouteResponse> call, Throwable t) {
                Log.e("MyTag3",t.getMessage());
                showInternetErrorMessage();
            }
        });
    }

    private void showInternetErrorMessage(){
        Toast.makeText(this, R.string.network_error,Toast.LENGTH_SHORT).show();
    }


    private void drawWayPolyline(RouteResponse response){
        mDecodedPoints = new ArrayList<>();
        for(RouteResponse.Route route:response.routes){
            Log.d("MyTag3",route.overview_polyline.points);

                mDistanceTV.setText(route.legs.get(0).distance.text);

            List<LatLng> decodedPoints = Constance.decodePolyPoints(route.overview_polyline.points);
            for(LatLng latLng:decodedPoints) mDecodedPoints.add(latLng);
        }
            Log.d("MapTag2","standNormalLine");
            drawWaypoints(mDecodedPoints);

    }

    private void drawWaypoints(List<LatLng> decodedPoint){
        Polyline polyline =  mMap.addPolyline(wayLineOptions);
        polyline.setPoints(decodedPoint);
        if(mGameController.getWaypoint()!=null){
            mGameController.getWaypoint().remove();
        }
        mGameController.setWaypoint(polyline);


    }



}
