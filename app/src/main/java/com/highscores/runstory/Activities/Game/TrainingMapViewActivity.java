package com.highscores.runstory.Activities.Game;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highscores.runstory.Controller.GameController;
import com.highscores.runstory.Utility.Constance;
import com.highscores.runstory.Model.MapPoint.BasePoint;
import com.highscores.runstory.R;

public class TrainingMapViewActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GameController mGameController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tranning_map_view);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.big_map);
        mapFragment.getMapAsync(this);
        mGameController = GameController.instance();
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng currentPosition = new LatLng(getIntent().getDoubleExtra(RunningActivity.LAT,0),getIntent().getDoubleExtra(RunningActivity.LNG,0));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentPosition));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(17));
        createPolyline();
        putLocationLabel(currentPosition);
        putMoney();
        LatLng start = new LatLng(getIntent().getDoubleExtra(RunningActivity.LAT+"S",0),getIntent().getDoubleExtra(RunningActivity.LNG+"S",0));
        if(start.latitude!=0&&start.longitude!=0){
            putStartLabel(start);
        }
        putMarkers();
        putFinishPoint();
        if( mGameController.getTask()!=null){
            mGameController.getTask().getTaskController().changeMap(mMap);
        }

    }

    private void createPolyline(){
        Polyline polyline =  mMap.addPolyline(new PolylineOptions()
                .color(getResources().getColor(R.color.runPolyline)));
        polyline.setPoints(mGameController.getGameParameters().getPoints());
        standSupportPolylines();
    }

    private void standSupportPolylines() {
        if(mGameController.getWaypoint()!=null){
            Polyline line =  mMap.addPolyline(new PolylineOptions().color(mGameController.getWaypoint().getColor()));
            line.setPoints(mGameController.getWaypoint().getPoints());
        }
    }

    public void putLocationLabel(LatLng position){
        mMap.addMarker(new MarkerOptions()
                .icon(Constance.smallBitmap(this,R.drawable.current_position))
                .anchor(0.5f,0.5f)
                .flat(true)
                .position(position));

    }

    public void putMoney(){
        for(BasePoint point: mGameController.getMoneyPoints()){
            point.stand(mMap);
        }
    }

    public void putStartLabel(LatLng position){
        mMap.addMarker(new MarkerOptions()
                .icon(Constance.smallBitmap(this,R.drawable.start_position))
                .title(getString(R.string.start))
                .anchor(0.5f,0.5f)
                .position(position));
    }

    private void putMarkers(){
        for(MarkerOptions opt:mGameController.getMarkers()){
            mMap.addMarker(opt);
        }
    }

    private void putFinishPoint(){
        if(mGameController.getFinishPoint()!=null){
            mGameController.getFinishPoint().stand(mMap);
        }
    }
}
