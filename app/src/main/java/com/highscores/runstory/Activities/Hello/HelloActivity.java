package com.highscores.runstory.Activities.Hello;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.highscores.runstory.Activities.MainMenu.MainActivity;
import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.R;
import com.highscores.runstory.Services.MainGameService;

public class HelloActivity extends AppCompatActivity {

    EditText mNameET;
    EditText mHeightET;
    EditText mWidthET;

    Button mApplyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Player.instance(this).isFirstTime()){
            setContentView(R.layout.activity_hello);
        }else {
            setContentView(R.layout.activity_change_parameters);
        }

        mNameET = (EditText) findViewById(R.id.editName);
        mHeightET = (EditText) findViewById(R.id.editGrown);
        mWidthET = (EditText) findViewById(R.id.editWidth);

        mApplyButton = (Button) findViewById(R.id.apply_hello);

        mApplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean allNormal = true;
                //TODO:выбрать красивые цвета для этой цели
                if(mNameET.getText().toString().length()==0){
                    mNameET.setBackgroundColor(getResources().getColor(R.color.menuButtonRed));
                    allNormal = false;
                }
                if(mHeightET.getText().toString().length()==0){
                    mHeightET.setBackgroundColor(getResources().getColor(R.color.menuButtonRed));
                    allNormal = false;
                }
                if(mWidthET.getText().toString().length()==0){
                    mWidthET.setBackgroundColor(getResources().getColor(R.color.menuButtonRed));
                    allNormal = false;
                }
                if(!allNormal)return;
                Player.instance(HelloActivity.this).setName(mNameET.getText().toString());

                Player.instance(HelloActivity.this).setGrown((int)Double.parseDouble(mHeightET.getText().toString()));
                Player.instance(HelloActivity.this).setWidth((int)Double.parseDouble(mWidthET.getText().toString()));
                Player.instance(HelloActivity.this).isNotFirstTime();

                Intent intent = new Intent(HelloActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
