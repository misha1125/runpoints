package com.highscores.runstory.Activities.MainMenu.Fragments.Statistic;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.highscores.runstory.Adapters.TrainingsAdapter;
import com.highscores.runstory.Model.Training.Training;
import com.highscores.runstory.Utility.TrainingSaver;
import com.highscores.runstory.R;

import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticFragment extends Fragment {


    public StatisticFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_statistic, container, false);
        // Inflate the layout for this fragment

        RecyclerView trainingsRV = (RecyclerView) rootView.findViewById(R.id.trainings_rv);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        trainingsRV.setLayoutManager(layoutManager);
        trainingsRV.addItemDecoration(new
                DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
        List<Training> trainings = TrainingSaver.instance(getActivity()).getTrainings();
        Collections.reverse(trainings);
        trainingsRV.setAdapter(new TrainingsAdapter(trainings,getActivity()));
        return rootView;
    }

}
