package com.highscores.runstory.Activities.Game;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highscores.runstory.Activities.MainMenu.Fragments.Values.MenuFragment;
import com.highscores.runstory.Activities.MainMenu.MainActivity;
import com.highscores.runstory.Controller.GameController;
import com.highscores.runstory.Fragments.ParametersDisplayFragment;
import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Model.GameParameters.GameParametersTypes;
import com.highscores.runstory.Utility.Constance;
import com.highscores.runstory.Model.MapPoint.MoneyPoint;

import com.highscores.runstory.R;
import com.highscores.runstory.Utility.TimeFormater;

import java.util.ArrayList;
import java.util.List;

public class RunningActivity extends AppCompatActivity implements GameView {


    public static final String TASK_NUM = "taskNum";
    public static final String WORK_SERVICE = "WS";
    public static final String ONLY_TIME = "only_time";
    public static final String LAT= "lat";
    public static final String LNG= "lng";
    private GoogleMap mMap;
    boolean isPressed = false;
    MenuFragment fragment1;
    android.app.FragmentTransaction transaction;

    Button mStartButton;
    View mPowerButtons;
    Button mPauseButton;
    Button mStopButton;

    List<ParametersDisplayFragment> mDisplayFragments;

    TextView mTaskTV;
    Polyline mPolyline;
    Marker locationMarker;

    GameController mGameController;
    LatLng mCurrentPosition;

    LatLng startPosition;

    Polyline mFinishPolyline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_running);

        applyMaps();
        initializeDisplayFragments();

        mStartButton = (Button) findViewById(R.id.start_training_button);
        mPowerButtons = findViewById(R.id.power_buttons);
        mPauseButton = (Button) findViewById(R.id.pause_training_buton);
        mStopButton = (Button) findViewById(R.id.stop_training_button);
        mTaskTV = (TextView) findViewById(R.id.task_text);


        setListeners();
        mGameController = GameController.instance(this,this);

    }

    private void initializeDisplayFragments(){
        initDisplayFragments();
        applyDisplayFragments();
    }

    private void initDisplayFragments(){
        mDisplayFragments = new ArrayList<>();

        ParametersDisplayFragment fr = new ParametersDisplayFragment(){
            {
                //setName("fffff");
            }
        };

        mDisplayFragments.add(((ParametersDisplayFragment) getFragmentManager()
                .findFragmentById(R.id.top_parameters)));
        mDisplayFragments.add((ParametersDisplayFragment) getFragmentManager()
                .findFragmentById(R.id.top_right_parameters));
        mDisplayFragments.add((ParametersDisplayFragment) getFragmentManager()
                .findFragmentById(R.id.bottom_left_parameters));
        mDisplayFragments.add((ParametersDisplayFragment) getFragmentManager()
                .findFragmentById(R.id.bottom_center_parameters));
        mDisplayFragments.add((ParametersDisplayFragment) getFragmentManager()
                .findFragmentById(R.id.bottom_right_parameters));
    }

    private void applyDisplayFragments(){
        mDisplayFragments.get(0).setName(getString(R.string.distance));
        mDisplayFragments.get(1).setName(getString(R.string.coins));
        mDisplayFragments.get(2).setName(getString(R.string.time));
        mDisplayFragments.get(3).setName(getString(R.string.calories));
        mDisplayFragments.get(4).setName(getString(R.string.speed));

        mDisplayFragments.get(0).setType(GameParametersTypes.distance);
        mDisplayFragments.get(1).setType(GameParametersTypes.coins);
        mDisplayFragments.get(2).setType(GameParametersTypes.time);
        mDisplayFragments.get(3).setType(GameParametersTypes.calories);
        mDisplayFragments.get(4).setType(GameParametersTypes.speed);
    }


    @Override
    public void onBackPressed() {
        if(!mGameController.isStarted()){
            mGameController.shoutdown();
            Intent intent = new Intent(this,MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }

    }

    private void applyMaps(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                applySmallMap(googleMap);
            }
        });
    }


    private void applySmallMap(GoogleMap googleMap){
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.zoomTo(17));
        standSupportPolylines();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                showBigMap();
            }
        });
        mMap.getUiSettings().setAllGesturesEnabled(false);
        putMarkers();
        if( mGameController.getTask()!=null){
            mGameController.getTask().getTaskController().setMainMap(mMap);
        }

    }


    private void standSupportPolylines() {
        if(mGameController.getWaypoint()!=null){
           Polyline line =  mMap.addPolyline(new PolylineOptions().color(mGameController.getWaypoint().getColor()));
           line.setPoints(mGameController.getWaypoint().getPoints());
        }

    }

    private void showBigMap() {
        if(mCurrentPosition == null){

            Toast.makeText(RunningActivity.this,getString(R.string.we_did_not_find),Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(RunningActivity.this,TrainingMapViewActivity.class);
        intent.putExtra(LAT,mCurrentPosition.latitude);
        intent.putExtra(LNG,mCurrentPosition.longitude);
        if(startPosition!=null){
            intent.putExtra(LAT+"S",startPosition.latitude);
            intent.putExtra(LNG+"S",startPosition.longitude);
        }
        startActivity(intent);
    }


    @Override
    public void putStopLabel(LatLng position){
        MarkerOptions opt = new MarkerOptions()
                .icon(Constance.bitmapScale(this,R.drawable.stop_marker,3))
                .title(getString(R.string.stop_place))
                .position(position);
        mMap.addMarker(opt);

        mGameController.addMarkerOption(opt);
    }

    @Override
    public void showPowerButtons() {
        mPowerButtons.setVisibility(View.VISIBLE);
        mStartButton.setVisibility(View.INVISIBLE);
        mStartButton.setText(R.string.continue_running);
        mTaskTV.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hidePowerButtons() {
        mPowerButtons.setVisibility(View.INVISIBLE);
        mStartButton.setVisibility(View.VISIBLE);
        mTaskTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void printTask(String test) {

    }

    @Override
    public void printCongratulations() {
        findViewById(R.id.task_complited_view).setVisibility(View.VISIBLE);
        if(mFinishPolyline!=null){
            mFinishPolyline.remove();
        }

    }


    private void putMarkers(){
        for(MarkerOptions opt:mGameController.getMarkers()){
            mMap.addMarker(opt);
        }
    }

    boolean haveStartLabel;

    @Override
    public void putStartLabel(LatLng position){
        if(!haveStartLabel){
            startPosition = position;
            MarkerOptions opt = new MarkerOptions()
                    .icon(Constance.smallBitmap(this,R.drawable.start_position))
                    .anchor(0.5f,0.5f)
                    .title(getString(R.string.start))
                    .position(position);
            mMap.addMarker(opt);
            mGameController.addMarkerOption(opt);
            haveStartLabel = true;
        }

    }

    public void putLocationLabel(LatLng position){
        locationMarker = mMap.addMarker(new MarkerOptions()
                .icon(Constance.smallBitmap(this,R.drawable.current_position))
                .anchor(0.5f,0.5f)
                .flat(true)
                .position(position));

    }



    public void updateLocationLabel(LatLng position) {
        if(locationMarker!=null){
            locationMarker.remove();
            Log.d("MapTag","invisible");
        }else {
            Log.d("MapTag","visible");
        }
        putLocationLabel(position);
    }

    //control buttons
    private void setListeners() {
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGameController.switchToActiveState();
            }
        });

        mPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGameController.pauseTraining();
            }
        });

        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGameController.endTraining();
            }
        });
    }




    @Override
    public void updateUI(GameParameters gameParameters) {
        for (ParametersDisplayFragment fragment:mDisplayFragments) {
            fragment.UpdateUI(gameParameters.toMap());
        }
    }

    @Override
    public void updateMap(boolean isRunning, List<LatLng> points, LatLng position){
        mCurrentPosition = position;
        if(isRunning){
            if(mPolyline == null){
                createPolyline();
            }
            mPolyline.setPoints(points);
        }
        updateLocationLabel(position);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
    }

    @Override
    public void onGetPositionFirstTime(LatLng position){
        mGameController.createMoneyPoints();
    }

    @Override
    public void standMoneyPoint(MoneyPoint point){
        point.standToMain(mMap);
    }

    private void createPolyline(){
        mPolyline =  mMap.addPolyline(new PolylineOptions()
                .color(getResources().getColor(R.color.runPolyline)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGameController.shoutdown();
    }
}
