package com.highscores.runstory.Activities.MainMenu.Fragments.Shop;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.highscores.runstory.Adapters.AchievementsAdapter;
import com.highscores.runstory.Adapters.ShopAdapter;
import com.highscores.runstory.Model.Achievements.AchievementsList;
import com.highscores.runstory.Model.Task.TasksList;
import com.highscores.runstory.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopFragment extends Fragment {

    RecyclerView mShopRV;
    public ShopFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_shop, container, false);
        mShopRV = (RecyclerView) rootView.findViewById(R.id.shop_rv);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mShopRV.setLayoutManager(layoutManager);
        mShopRV.setAdapter(new ShopAdapter(TasksList.getLastTacks(getActivity()),getActivity()));
        return rootView;
    }

}
