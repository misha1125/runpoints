package com.highscores.runstory.Activities.MainMenu.Fragments.MyPage;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.highscores.runstory.Activities.Hello.HelloActivity;
import com.highscores.runstory.Model.Player.LevelUpParameters;
import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPageFragment extends Fragment {

    TextView mCoinsTV;
    TextView mDistanceTV;
    TextView mMaxDistanceTV;
    TextView mMaxSpeedTV;
    TextView mCaloriesTV;

    TextView mNameTV;
    TextView mHeightTV;
    TextView mWidthTV;

    TextView mLevelTV;
    TextView mNeedTV;
    ProgressBar mExperiencePB;

    Button mChangeButton;

    public MyPageFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_page, container, false);

        mCoinsTV = (TextView) rootView.findViewById(R.id.total_moneys_view_mp);
        mDistanceTV = (TextView) rootView.findViewById(R.id.distance_view_mp);
        mMaxDistanceTV = (TextView) rootView.findViewById(R.id.max_distance_view_my_page);
        mMaxSpeedTV = (TextView) rootView.findViewById(R.id.max_speed_view_mp);
        mCaloriesTV = (TextView) rootView.findViewById(R.id.total_calories_mp);

        mNameTV = (TextView) rootView.findViewById(R.id.name_view_mp);
        mHeightTV = (TextView) rootView.findViewById(R.id.hieght_view_mp);
        mWidthTV = (TextView) rootView.findViewById(R.id.wight_view_mp);

        mLevelTV = (TextView) rootView.findViewById(R.id.level_view_mp);
        mNeedTV = (TextView) rootView.findViewById(R.id.level_next_mp);
        mExperiencePB = (ProgressBar) rootView.findViewById(R.id.level_progress_mp);

        mChangeButton = (Button) rootView.findViewById(R.id.change_parameters_mp);
        Player player = Player.instance(getActivity());

        mCoinsTV.setText(String.valueOf(player.getCoinsCount()));
        mDistanceTV.setText(String.format("%.2f",player.getDistance()));
        mMaxDistanceTV.setText(String.format("%.2f",player.getMaxDistance()));
        mMaxSpeedTV.setText(String.format("%.2f",player.getMaxSpeed()));
        mCaloriesTV.setText(String.valueOf(player.getCalories()));

        mNameTV.setText(getString(R.string.user_name) + " " +player.getName());
        mHeightTV.setText(getString(R.string.user_grown)+ " " +String.valueOf(player.getGrown())+ " " + (getString(R.string.sm)));
        mWidthTV.setText(getString(R.string.user_weight)+ " " +String.valueOf(player.getWidth())+ " " + (getString(R.string.kg)));

        mLevelTV.setText(getString(R.string.User_level)+ " " +String.valueOf(player.getLevel()));

        mNeedTV.setText(getString(R.string.next_level_text) +String.format("%.2f",LevelUpParameters.getDistanceToCompleteLevel(player.getLevel())-player.getDistance()) + " " +getString(R.string.km));

        mExperiencePB.setMax((int)((LevelUpParameters.getDistanceToCompleteLevel(player.getLevel()) - LevelUpParameters.getDistanceToCompleteLevel(player.getLevel()-1))*1000));
        Log.d("MyPage",(LevelUpParameters.getDistanceToCompleteLevel(player.getLevel()) - LevelUpParameters.getDistanceToCompleteLevel(player.getLevel()-1)) + "");
        mExperiencePB.setProgress((int)((player.getDistance() - (LevelUpParameters.getDistanceToCompleteLevel(player.getLevel()-1)))*1000));
        Log.d("MyPage",(player.getDistance() - (LevelUpParameters.getDistanceToCompleteLevel(player.getLevel()-1)))+ "");
        mChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), HelloActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }

}
