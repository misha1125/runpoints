package com.highscores.runstory.Activities.Game;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Model.MapPoint.MoneyPoint;

import java.util.List;

/**
 * Created by михаил on 09.05.2017.
 */

public interface GameView {
    void updateUI(GameParameters gameParameters);
    void updateMap(boolean isRunning, List<LatLng> points, LatLng position);
    void onGetPositionFirstTime(LatLng position);
    void standMoneyPoint(MoneyPoint point);
    void putStartLabel(LatLng position);
    void putStopLabel(LatLng position);

    void showPowerButtons();
    void hidePowerButtons();

    void printTask(String test);
    void printCongratulations();
}
