package com.highscores.runstory.Activities.MainMenu.Fragments.Achievements;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.highscores.runstory.Adapters.AchievementsAdapter;
import com.highscores.runstory.Model.Achievements.AchievementsList;
import com.highscores.runstory.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AchievementsFragment extends Fragment {


    RecyclerView mAchievementsRV;
    public AchievementsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_achievements, container, false);
        mAchievementsRV = (RecyclerView) rootView.findViewById(R.id.achievement_rv);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mAchievementsRV.setLayoutManager(layoutManager);
        mAchievementsRV.addItemDecoration(new
                DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
        mAchievementsRV.setAdapter(new AchievementsAdapter(AchievementsList.getAchievementsList(getActivity()),getActivity()));
        return rootView;
    }

}
