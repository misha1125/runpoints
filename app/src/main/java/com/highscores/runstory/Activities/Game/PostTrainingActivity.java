package com.highscores.runstory.Activities.Game;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highscores.runstory.Activities.MainMenu.MainActivity;
import com.highscores.runstory.Controller.GameController;
import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Utility.TimeFormater;
import com.highscores.runstory.Utility.TrainingSaver;
import com.highscores.runstory.Model.Training.Training;
import com.highscores.runstory.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Collections;
import java.util.List;

public class PostTrainingActivity extends AppCompatActivity {

    int trainingIndex;
    Training mTraining;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_training);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAndClose();
            }
        });


        CheckTraining();

        ApplyGraph();
        ApplyMap();
        ApplyUI();

    }

    private void CheckTraining() {
        trainingIndex = getIntent().getIntExtra("tr",-1);

        if(trainingIndex == -1){
            mTraining = new Training();
            GameParameters parameters = GameController.instance().getGameParameters();
            mTraining.gameParameters = parameters;
        }
        else {
            List<Training> tr =  TrainingSaver.instance(this).getTrainings();
            Collections.reverse(tr);
            mTraining =tr.get(trainingIndex);
            setTitle(getString(R.string.name_activity_running) + ", " + TimeFormater.formatDate(mTraining.date));
        }
    }

    private void ApplyUI() {
        ((TextView) findViewById(R.id.distance_rez)).setText(String.format("%.2f",mTraining.gameParameters.getDistance()));
        ((TextView) findViewById(R.id.calories_rez)).setText(String.valueOf((int)mTraining.gameParameters.getCalories()));
        ((TextView) findViewById(R.id.time_rez)).setText(TimeFormater.formatTime(mTraining.gameParameters.getTime()));

        ((TextView) findViewById(R.id.max_speed_rez)).setText(String.format("%.2f", +mTraining.gameParameters.getMaxSpeed()) + getString(R.string.mm_hour));
        ((TextView) findViewById(R.id.midle_rez)).setText(String.format("%.2f",mTraining.gameParameters.getMiddleSpeed()) + getString(R.string.mm_hour));
        ((TextView) findViewById(R.id.money_rez)).setText(String.valueOf(mTraining.gameParameters.getCoins()));
    }

    private void ApplyGraph() {
        GraphView graph = (GraphView) findViewById(R.id.graph);
        DataPoint[] speed = new DataPoint[mTraining.gameParameters.speedsList.size()];
        mTraining.gameParameters.speedsList.toArray(speed);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(speed);
        graph.addSeries(series);
    }

    private void ApplyMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.rez_map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                applySmallMap(googleMap);
            }
        });
    }

    void  applySmallMap(GoogleMap map){
        Polyline polyline = map.addPolyline(new PolylineOptions().color(getResources().getColor(R.color.runPolyline)));
        polyline.setPoints(mTraining.gameParameters.getPoints());
        double latSum = 0;
        double lngSum = 0;
        for (LatLng point:mTraining.gameParameters.getPoints()){
            latSum += point.latitude;
            lngSum += point.longitude;
        }

        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latSum/mTraining.gameParameters.getPoints().size(),lngSum/mTraining.gameParameters.getPoints().size())));
        map.moveCamera(CameraUpdateFactory.zoomTo(13f));
    }

    private void saveAndClose(){
        if(trainingIndex == -1){
            saveTrainingInBD();
        }
        startMainActivity();
    }

    private void startMainActivity() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void saveTrainingInBD(){
        TrainingSaver.instance(this).saveTraining(mTraining);
    }


    @Override
    public void onBackPressed() {
        saveAndClose();
    }
}
