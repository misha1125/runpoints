package com.highscores.runstory.Activities.MainMenu.Fragments.Values;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.highscores.runstory.Adapters.TrainingsAdapter;
import com.highscores.runstory.Model.Training.Training;
import com.highscores.runstory.R;
import com.highscores.runstory.Utility.TrainingSaver;

import java.util.Collections;
import java.util.List;

/**
 * Created by Rennnat on 28.08.2017.
 */

public class MenuFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       return inflater.inflate(R.layout.activity_running_menu, null);
    }
}
