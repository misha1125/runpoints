package com.highscores.runstory.Fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.R;

import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */



public class ParametersDisplayFragment extends Fragment {


    TextView mValueView;
    TextView mTitleView;

    private String title;
    private String type;

    public ParametersDisplayFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_parameters_display, container, false);
        mValueView = (TextView) rootView.findViewById(R.id.parameter_value);
        mTitleView = (TextView) rootView.findViewById(R.id.parameter_title);
        return rootView;
    }

    public void UpdateUI(Map<String,String> parameters){
        mValueView.setText(parameters.get(type));
    }


    public void setName(String title) {
        this.title = title;
        mTitleView.setText(title);
    }

    public void setType(String type) {
        this.type = type;
    }
}
