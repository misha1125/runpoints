package com.highscores.runstory.Controller.Task;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highscores.runstory.API.Response.RouteResponse;
import com.highscores.runstory.API.RouteApi;
import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Model.MapPoint.FinishPoint;
import com.highscores.runstory.Model.Task.RunToPointTask;
import com.highscores.runstory.R;
import com.highscores.runstory.Utility.Constance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.highscores.runstory.Activities.Game.CreateTrainingActivity.DIRECTIONS_BASE_LINK;

/**
 * Created by михаил on 07.08.2017.
 */

public class RunToPointController extends TaskController {

    RunToPointTask mTask;

    FinishPoint mFinishPoint;
    Polyline mPolyline;
    PolylineOptions finishLineOptions;

    public RunToPointController(Context context,RunToPointTask task) {
        super(context);
        mTask = task;
        finishLineOptions = new PolylineOptions().geodesic(true);
        finishLineOptions.color(mContext.getResources().getColor(R.color.finishPolyline));
    }

    @Override
    public void init(LatLng currentPosition) {
        super.init(currentPosition);
        if(mMap!=null){
            putDistancePoint();
        }
    }

    @Override
    public void checkAndChangeParameters(GameParameters parameters) {
        super.checkAndChangeParameters(parameters);
        if(mFinishPoint!=null){
            if(mFinishPoint.checkPoint(parameters.currentPosition)){
                parameters.increaseCoins(mTask.getCost());
                mTask.nextTask();
                init(parameters.currentPosition);
            }
        }
    }

    @Override
    public void changeMap(GoogleMap map) {
        super.changeMap(map);
        if(mFinishPoint!=null){
            mFinishPoint.stand(map);
        }
    }

    @Override
    public void setMainMap(GoogleMap map) {
        super.setMainMap(map);
        if(mFinishPoint!=null){
            mFinishPoint.standToMain(map);
        }
    }

    private void putDistancePoint(){
        getWayPoliline(Constance.randomLatLng(mPosition,mTask.getCurrentValue()));
    }

    private void getWayPoliline(LatLng end){
        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(DIRECTIONS_BASE_LINK)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RouteApi mApi = mRetrofit.create(RouteApi.class);
        mApi.getRoute(Constance.formatLatLng(mPosition),
                Constance.formatLatLng(end),
                true,"ru","walking","").enqueue(new Callback<RouteResponse>() {

            @Override
            public void onResponse(Call<RouteResponse> call, Response<RouteResponse> response) {
                drawWayPolyline(response.body());
                Log.d("MyTag3","ok");
            }

            @Override
            public void onFailure(Call<RouteResponse> call, Throwable t) {
                Log.e("MyTag3",t.getMessage());
                showInternetErrorMessage();
            }
        });
    }

    private void showInternetErrorMessage(){
        Toast.makeText(mContext,"Ошибка подключения к интернету",Toast.LENGTH_SHORT).show();
    }

    private void drawWayPolyline(RouteResponse response){
        List<LatLng> mDecodedPoints = new ArrayList<>();
        for(RouteResponse.Route route:response.routes){
            Log.d("MyTag3",route.overview_polyline.points);
            List<LatLng> decodedPoints = Constance.decodePolyPoints(route.overview_polyline.points);
            for(LatLng latLng:decodedPoints) mDecodedPoints.add(latLng);
        }

         drawWaypointsWihtFinish(mDecodedPoints);

    }

    private void drawWaypointsWihtFinish(List<LatLng> decodedPoints){
        float dist = mTask.getCurrentValue();
        int currentIndex = 1;
        while (dist>0&&currentIndex<decodedPoints.size()-1){
            dist-=Constance.distance(decodedPoints.get(currentIndex-1),decodedPoints.get(currentIndex));
            ++currentIndex;
        }
        mFinishPoint = new FinishPoint(mContext);
        mFinishPoint.setPosition(decodedPoints.get(currentIndex));
        mFinishPoint.stand(mMap);

        mPolyline =  mMap.addPolyline(finishLineOptions);
        mPolyline.setPoints(decodedPoints.subList(0,currentIndex+1));
        mFinishPoint.setPolyne(mPolyline);
    }
}


