package com.highscores.runstory.Controller.Task;

import android.content.Context;
import android.util.Log;

import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Model.Task.RunTimeTask;

/**
 * Created by михаил on 17.08.2017.
 */

public class RunTimeController extends TaskController {

    private RunTimeTask mTask;

    private boolean mIsCompleted;

    public RunTimeController(Context context,RunTimeTask task) {
        super(context);
        mTask = task;
    }

    @Override
    public void initParameters(GameParameters parameters) {
        super.initParameters(parameters);
        parameters.setTime(mTask.getCurrentValue());
    }

    @Override
    public void checkAndChangeParameters(GameParameters parameters) {
        super.checkAndChangeParameters(parameters);
        if(parameters.getTime()<mTask.getCurrentValue()){
            parameters.setTime(mTask.getCurrentValue() - parameters.getTime());
        }else {
            if(!mIsCompleted){
                mIsCompleted = true;
                parameters.increaseCoins(mTask.getCost());
                mTask.nextTask();
            }
        }
    }
}
