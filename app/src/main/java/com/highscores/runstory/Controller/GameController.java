package com.highscores.runstory.Controller;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.highscores.runstory.Activities.Game.GameView;
import com.highscores.runstory.Activities.Game.PostTrainingActivity;
import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Utility.Constance;
import com.highscores.runstory.Model.MapPoint.FinishPoint;
import com.highscores.runstory.Model.MapPoint.MoneyPoint;
import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.Model.Task.Task;
import com.highscores.runstory.R;
import com.highscores.runstory.Services.MainGameService;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;

import static com.highscores.runstory.Activities.Game.RunningActivity.ONLY_TIME;
import static com.highscores.runstory.Activities.Game.RunningActivity.WORK_SERVICE;
import static com.highscores.runstory.Services.GameDisplay.DISTANCE;
import static com.highscores.runstory.Services.GameDisplay.GAME_SERVICE_INTENT;
import static com.highscores.runstory.Services.GameDisplay.LAT;
import static com.highscores.runstory.Services.GameDisplay.LNG;
import static com.highscores.runstory.Services.GameDisplay.SPEED;
import static com.highscores.runstory.Services.GameDisplay.TIME;

/**
 * Created by михаил on 09.05.2017.
 */

public class GameController {

    private static GameController currentController;
    private Context mContext;

    private GameView mGameView;
    private GameParameters mGameParameters;

    private Task mTask;

    private boolean mIsRunning;

    public boolean isStarted() {
        return mIsStarted;
    }

    private boolean mIsStarted;

    private Intent mServiceIntent;
    private BroadcastReceiver mBroadcastReceiver;

    private boolean mHasActiveService;

    private double mLat;
    private double mLng;


    FinishPoint mFinishPoint;

    final int MONEY_POINTS_COUNT = 5;
    private boolean fake;

    private Polyline waypoint;

    public GameParameters getGameParameters() {
        return mGameParameters;
    }

    public Task getTask() {
        return mTask;
    }

    public void setTask(Task task) {
        mTask = task;

    }

    public FinishPoint getFinishPoint() {
        return mFinishPoint;
    }

    public ArrayList<MoneyPoint> getMoneyPoints() {
        return mMoneyPoints;
    }

    private ArrayList<MoneyPoint> mMoneyPoints = new ArrayList<>();

    public ArrayList<MarkerOptions> getMarkers() {
        return mMarkers;
    }

    private ArrayList<MarkerOptions>mMarkers = new ArrayList<>();



    public void addMarkerOption(MarkerOptions options){
        mMarkers.add(options);
    }

    private GameController(Context context,GameView gameView){
        mContext = context;
        mGameView = gameView;
        mGameParameters = new GameParameters();
        startTrainingService(false);

    }

    private GameController(Context context,GameView gameView,boolean fake){
        mContext = context;
        mGameView = gameView;
        this.fake = fake;
        if(!fake)
            startTrainingService(false);
    }


    public static GameController instance(Context context,GameView gameView){
        if(currentController==null){
            currentController = new GameController(context,gameView);
        }else {
            currentController.resetController();
            currentController.mContext = context;
            currentController.mGameView = gameView;
            currentController.applyPowerButtons();
            if(currentController.mTask!=null){
                currentController.mGameView.printTask(currentController.mTask.getDescription());
            }
            currentController.startTrainingService(currentController.mIsRunning);
            currentController.fake = false;
        }
        if(currentController.mTask!=null){
            currentController.mTask.getTaskController().initParameters(currentController.mGameParameters);

        }
        gameView.updateUI(currentController.mGameParameters);
        return currentController;
    }


    private void applyPowerButtons() {
        if(mIsStarted){
            mGameView.showPowerButtons();
            if(!mIsRunning){
                mGameView.hidePowerButtons();
            }
        }
    }

    public static GameController instance(){
        return currentController;
    }

    public Polyline getWaypoint() {
        return waypoint;
    }

    public void setWaypoint(Polyline waypoint) {
        this.waypoint = waypoint;
    }




    private void resetController(){
        mFirstTime = true;
        stopTrainingService();
        mGameParameters = new GameParameters();
    }


    private void startTrainingService(boolean work){
        startServiceWithStartParameters(mGameParameters.getTime(),mGameParameters.getDistance(),work);
        connectToServiceWithBroadcastReceiver();
        mHasActiveService = true;
    }

    private void startServiceWithStartParameters(int time,double distance,boolean work){
        mServiceIntent = new Intent(mContext, MainGameService.class);
        mServiceIntent.putExtra(DISTANCE,distance);
        mServiceIntent.putExtra(TIME,time);
        mServiceIntent.putExtra(WORK_SERVICE,work);
        mContext.startService(mServiceIntent);
    }

    private void restartTrainingService(){
        stopTrainingService();
        startTrainingService(true);
    }

    private void stopTrainingService(){
        if(mHasActiveService){
            mContext.stopService(mServiceIntent);
            if(!fake){
                mContext.unregisterReceiver(mBroadcastReceiver);
            }

            mHasActiveService = false;
        }
    }


    private void stop(){
        stopTrainingService();
    }
    public void shoutdown(){
        stop();
        currentController = new GameController(currentController.mContext,currentController.mGameView,true);
        currentController.stop();
    }

    private boolean mFirstTime = true;
    private void connectToServiceWithBroadcastReceiver(){
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                mLat = intent.getDoubleExtra(LAT,0);
                mLng = intent.getDoubleExtra(LNG,0);

                mGameParameters.currentPosition = new LatLng(mLat,mLng);

                if(mIsRunning){

                    mGameParameters.setTime(intent.getIntExtra(TIME,0));

                    if(!intent.getBooleanExtra(ONLY_TIME,false)) {
                        mGameParameters.setDistance(intent.getDoubleExtra(DISTANCE, 0));
                        mGameParameters.setSpeed(intent.getFloatExtra(SPEED, 0));
                        mGameParameters.speedsList.add(new DataPoint(mGameParameters.getTime()/60f,mGameParameters.getSpeed()));
                        Player.instance(mContext).checkMaxSpeed(mGameParameters.getSpeed());
                    }

                    mGameParameters.setCalories(mGameParameters.getDistance() * Player.instance(mContext).getWidth());

                    if (mTask != null) {
                        mTask.getTaskController().checkAndChangeParameters(mGameParameters);
                    }
                    mGameView.updateUI(mGameParameters);
                    if( mLat!=0&& mLng!=0){
                        mGameParameters.getPoints().add(new LatLng(mLat,mLng));
                    }

                }
                if( mLat!=0&& mLng!=0){
                    mGameParameters.currentPosition = new LatLng(mLat,mLng);
                    mGameView.updateMap(mIsRunning, mGameParameters.getPoints(),new LatLng(mLat,mLng));
                    if(mFirstTime){
                        mGameView.onGetPositionFirstTime(new LatLng(mLat,mLng));
                        mFirstTime = false;
                    }
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter(GAME_SERVICE_INTENT);
        mContext.registerReceiver(mBroadcastReceiver,intentFilter);
    }





    public void switchToActiveState(){
        if(!mIsRunning){
            if(mGameParameters.currentPosition!=null){
                mIsRunning = true;
                mGameView.showPowerButtons();
                mGameView.putStartLabel(mGameParameters.currentPosition);
                restartTrainingService();
                mIsStarted = true;
            }else {
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mContext);
                dlgAlert.setMessage("Возможно вы забыли включить определение местоположения.\nВ этом случае включите его в настройках.");
                dlgAlert.setTitle(mContext.getString(R.string.we_did_not_find));
                dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
            }

        }
    }
    public void pauseTraining(){
        mIsRunning = false;
        mGameView.hidePowerButtons();
        stopTrainingService();
    }
    public void endTraining(){
        applyPlayerParameters();
        stopTrainingService();
        Intent intent = new Intent(mContext, PostTrainingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
    }

    private void applyPlayerParameters(){
        Player.instance(mContext).increaseDistance((float) mGameParameters.getDistance());
        Player.instance(mContext).increaseCoinsCount((int)(mGameParameters.getDistance() * Constance.MONEYS_PER_KM));
        Player.instance(mContext).increaseCalories((int)mGameParameters.getCalories());
    }

    public void createMoneyPoints(){
        if(mMoneyPoints.size()==0) {
            for (int i = 0; i < MONEY_POINTS_COUNT; ++i) {
                mMoneyPoints.add(createMoneyPoint());
            }
        }
        else {
            for (int i = 0; i < MONEY_POINTS_COUNT; ++i) {
                mGameView.standMoneyPoint(mMoneyPoints.get(i));
            }
        }
        checkAllPoints();
    }

    private MoneyPoint createMoneyPoint(){
        MoneyPoint point = new MoneyPoint(20,mContext);
        point.setPosition(Constance.randomLatLng(new LatLng(mLat,mLng),Constance.MAX_MONEY_CREATING_RADIUS,Constance.MIN_POINT_CREATING_RADIUS));
        mGameView.standMoneyPoint(point);
        return point;
    }

    private void checkAllPoints() {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(mIsRunning){
                    for(int i = 0;i<mMoneyPoints.size();++i){
                        try {
                            if(mMoneyPoints.get(i).checkPoint(new LatLng(mLat,mLng))){
                                mGameParameters.increaseCoins(mMoneyPoints.get(i).getProfit());
                                mMoneyPoints.add(createMoneyPoint());
                                mMoneyPoints.remove(i);
                                --i;
                                Log.d("MyTag2","colected");
                            }
                        }catch (Exception e){
                            Log.e("MyTag2","money error "+e.getMessage());
                        }
                    }
                }
                handler.postDelayed(this, 1000);
            }
        });

    }


}
