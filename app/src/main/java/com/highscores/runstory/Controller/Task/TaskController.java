package com.highscores.runstory.Controller.Task;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.highscores.runstory.Model.GameParameters.GameParameters;
import com.highscores.runstory.Utility.Constance;

/**
 * Created by михаил on 06.08.2017.
 */

public abstract class TaskController {

    GoogleMap mMap;
    Context mContext;
    LatLng mPosition;

    public TaskController(Context context){
        mContext = context;
    }

    public void init(LatLng currentPosition){
        mPosition = currentPosition;
    }

    public void changeMap(GoogleMap map){
        mMap = map;
    }

    public void setMainMap(GoogleMap map){
        mMap = map;
    }

    public float getProgress(){
        return 0;
    }

    public void initParameters(GameParameters parameters){

    }

    public void checkAndChangeParameters(GameParameters parameters){

    }
}
