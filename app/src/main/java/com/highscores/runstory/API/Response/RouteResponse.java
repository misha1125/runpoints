package com.highscores.runstory.API.Response;

import java.util.List;

/**
 * Created by михаил on 11.05.2017.
 */

public class RouteResponse {
    public List<Route> routes;

    public class Route {
        public  OverviewPolyline overview_polyline;
        public List<Leg> legs;
    }

    public class OverviewPolyline {
        public String points;
    }

    public class Leg{
        public Distance distance;
    }

    public class Distance
    {
        public String text;
        public long value;
    }
}
