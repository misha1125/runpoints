package com.highscores.runstory.API;

import com.highscores.runstory.API.Response.RouteResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by михаил on 11.05.2017.
 */

public interface RouteApi {
    @GET("/maps/api/directions/json")
    Call<RouteResponse> getRoute(
            @Query("origin") String position,
            @Query("destination") String destination,
            @Query("sensor") boolean sensor,
            @Query("language") String language,
            @Query("mode")String mode,
            @Query("waypoints")String waypoints);
}