package com.highscores.runstory.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highscores.runstory.Activities.Game.PostTrainingActivity;
import com.highscores.runstory.Model.Training.Training;
import com.highscores.runstory.R;
import com.highscores.runstory.Utility.TimeFormater;

import java.util.List;

/**
 * Created by михаил on 26.06.2017.
 */

public class TrainingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<Training> mTrainings;

    LayoutInflater lInflater;
    Activity mActivity;




    public TrainingsAdapter(List<Training> trainings, Activity activity) {
        mTrainings = trainings;
        lInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActivity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.training_preview, parent, false);
        TrainingHolder th = new TrainingHolder(v);
        return th;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        TrainingHolder vh = (TrainingHolder) holder;
        vh.mDistance.setText(String.format("%.2f",mTrainings.get(position).gameParameters.getDistance()) + " " + mActivity.getString(R.string.km));
        vh.mTime.setText(TimeFormater.formatTime(mTrainings.get(position).gameParameters.getTime()));
        vh.mDate.setText(TimeFormater.formatDate(mTrainings.get(position).date));
        vh.mMainElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, PostTrainingActivity.class);
                intent.putExtra("tr",position);
                mActivity.startActivity(intent);
            }
        });
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mTrainings.size();
    }


    public static class TrainingHolder extends RecyclerView.ViewHolder {
        public TextView mDistance;

        public TextView mTime;
        public TextView mDate;
        public View mMainElement;

        public TrainingHolder(View v) {
            super(v);
            mDistance = (TextView) v.findViewById(R.id.training_distance_pr);
            mTime = (TextView) v.findViewById(R.id.training_time_pr);
            mDate = (TextView) v.findViewById(R.id.training_date_pr);
            mMainElement = v;
        }
    }

}