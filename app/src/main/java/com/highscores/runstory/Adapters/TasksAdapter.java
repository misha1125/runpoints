package com.highscores.runstory.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.highscores.runstory.Activities.Game.CreateTrainingActivity;
import com.highscores.runstory.Activities.MainMenu.MainActivity;
import com.highscores.runstory.Model.Task.RunTimeTask;
import com.highscores.runstory.Model.Task.RunToPointTask;
import com.highscores.runstory.Model.Task.Task;
import com.highscores.runstory.R;
import com.highscores.runstory.Utility.TimeFormater;

import java.util.List;

/**
 * Created by михаил on 20.02.2017.
 */

public class TasksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<Task> mTasks;

    LayoutInflater lInflater;
    Activity mActivity;

    View _herder;

    static final int HERDER = -1;



    public TasksAdapter(List<Task> tasks, View herder, Activity activity) {
        mTasks = tasks;
        lInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActivity = activity;
        _herder = herder;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType != HERDER){
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.task_view, parent, false);

            TasksHolder th = new TasksHolder(v);
            return th;
        }
        else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.empty_item, parent, false);

            HerderHolder hh = new HerderHolder(v);
            return hh;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position !=0){
            TasksHolder vh = (TasksHolder) holder;
            final int pos = position;

            final Task task = mTasks.get(position-1);
            if(!task.isBought()){
                vh.mBegin.setText(mActivity.getString(R.string.to_shop));
                vh.mBegin.setScaleX(0.9f);
                vh.mBegin.setScaleY(0.8f);
                vh.mBegin.setBackgroundResource(R.drawable.gray_button);
                vh.mTitle.setText(task.getTitle());
            }else {
                vh.mTitle.setText(task.getTitle() + " " + task.getProgressString());
            }
            vh.mBegin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(task.isBought()){
                        Intent intent = new Intent(mActivity, CreateTrainingActivity.class);
                        intent.putExtra(Task.TASK_NUM,pos-1);
                        mActivity.startActivity(intent);
                    }else {
                        ((MainActivity)mActivity).goToShop();
                    }

                }
            });


            if(task instanceof RunToPointTask)
                vh.mCondition.setText(mActivity.getString(R.string.next_waypoint_in) + " " +  metersToDistance(task.getCurrentValue())  + " "+ mActivity.getString(R.string.about_you));
            else if(task instanceof RunTimeTask)
                vh.mCondition.setText(mActivity.getString(R.string.continue_training) +" " +  TimeFormater.formatTime(task.getCurrentValue())+ " " + mActivity.getString(R.string.min));

            vh.profit.setText(mActivity.getString(R.string.profit) + " " +task.getCost());
            vh.progress.setMax(task.tasksCount()-1);
            vh.progress.setProgress(task.currentProgress());
        }else {
            HerderHolder hh = (HerderHolder) holder;
            hh.container.addView(_herder);
        }
    }

    private String metersToDistance(int meters){
        if(meters<1000){
            return meters +" "+ mActivity.getString(R.string.metres);
        }else {
            return meters / 1000.0 +" "+ mActivity.getString(R.string.km);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return HERDER;
        }
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
         return mTasks.size()+1;
    }


    public static class TasksHolder extends RecyclerView.ViewHolder {
        public TextView mCondition;
        public Button mBegin;

        public TextView mTitle;
        public TextView profit;
        public ProgressBar progress;

        public TasksHolder(View v) {
            super(v);
            mCondition = (TextView)v.findViewById(R.id.task_description);
            mBegin = (Button) v.findViewById(R.id.begin_task);
            mTitle = (TextView)v.findViewById(R.id.task_name);
            profit = (TextView)v.findViewById(R.id.task_cost);
            progress = (ProgressBar)v.findViewById(R.id.task_progres);
        }
    }

    public static class HerderHolder extends RecyclerView.ViewHolder {
       public LinearLayout container;

        public HerderHolder(View v) {
            super(v);
           container = (LinearLayout) v.findViewById(R.id.empty_container);
        }
    }



}