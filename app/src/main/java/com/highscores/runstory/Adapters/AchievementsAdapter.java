package com.highscores.runstory.Adapters;

/**
 * Created by михаил on 06.06.2017.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highscores.runstory.Model.Achievements.Achievement;
import com.highscores.runstory.R;

import java.util.List;


public class AchievementsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<Achievement> mAchievements;

    LayoutInflater lInflater;
    Activity mActivity;




    public AchievementsAdapter(List<Achievement> achievements, Activity activity) {
        mAchievements = achievements;
        lInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActivity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.acivement_element, parent, false);
        AchievementHolder th = new AchievementHolder(v);
        return th;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AchievementHolder vh = (AchievementHolder) holder;
        vh.mTitle.setText(mAchievements.get(position).getName());
        vh.mDescription.setText(mAchievements.get(position).getDescription());
        if(mAchievements.get(position).isGot()) {
            vh.mIcon.setImageResource(mAchievements.get(position).getIcon());
        }else {
            vh.mIcon.setImageResource(Achievement.DEFAULT_ICON);
        }
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mAchievements.size();
    }


    public static class AchievementHolder extends RecyclerView.ViewHolder {
        public TextView mDescription;

        public TextView mTitle;
        public ImageView mIcon;
        public View mMainElement;

        public AchievementHolder(View v) {
            super(v);
            mDescription = (TextView) v.findViewById(R.id.achievement_deskription);
            mTitle = (TextView) v.findViewById(R.id.achievement_title);
            mIcon = (ImageView) v.findViewById(R.id.achievement_icon);
            mMainElement = v;
        }
    }

}