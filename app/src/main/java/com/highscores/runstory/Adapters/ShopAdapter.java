package com.highscores.runstory.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.Model.Task.Task;
import com.highscores.runstory.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by михаил on 22.06.2017.
 */

public class ShopAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<Task> mTasks;

    LayoutInflater lInflater;
    Activity mActivity;

    static final int HERDER = -1;



    public ShopAdapter(List<Task> tasks, Activity activity) {
        mTasks = tasks;
        lInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mActivity = activity;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_view_shop, parent, false);

        TasksHolder th = new TasksHolder(v);
        return th;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final TasksHolder vh = (TasksHolder) holder;

        if(mTasks.get(position).isBought()){
            vh.mBegin.setEnabled(false);
        }
        vh.mBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if(Player.instance(mActivity).getCoinsCount()>=mTasks.get(position).getPrise()){
                  mTasks.get(position).buy();
                  Player.instance(mActivity).increaseCoinsCount(-mTasks.get(position).getPrise());
                  vh.mBegin.setEnabled(false);
              }else {
                  printHaveNoMoney();
              }
            }
        });

        vh.mTitle.setText(mTasks.get(position).getTitle());
        vh.mCondition.setText(mTasks.get(position).getDescription());
        vh.mCost.setText(mActivity.getString(R.string.cost) + " "+ mTasks.get(position).getPrise());
    }

    void printHaveNoMoney(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
        dlgAlert.setTitle(mActivity.getString(R.string.not_enough_money));
        dlgAlert.setMessage(mActivity.getString(R.string.not_enough_money_full));
        dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mTasks.size();
    }




    public static class TasksHolder extends RecyclerView.ViewHolder {
        public TextView mCondition;
        public Button mBegin;
        public TextView mTitle;
        public TextView mCost;

        public TasksHolder(View v) {
            super(v);
            mCondition = (TextView)v.findViewById(R.id.task_description_shop);
            mBegin = (Button) v.findViewById(R.id.buy_task);
            mTitle = (TextView)v.findViewById(R.id.task_name_shop);
            mCost = (TextView)v.findViewById(R.id.task_cost_shop);

        }
    }



}