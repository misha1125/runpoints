package com.highscores.runstory.Model.Achievements;

import android.content.Context;

import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.R;

/**
 * Created by михаил on 22.04.2017.
 */

public class Achievement {
    private String mName;
    private String mDescription;
    private int mIconId;
    private boolean mIsGot;
    private float mValue;

    private int mType;

    public static final int DEFAULT_ICON = R.drawable.lock;
    public static class AchievementType{
        public static final int distance = 0;
        public static final int level = 1;
        public static final int maxDistance = 2;
        public static final int maxSpeed = 3;
        public static final int calories = 4;
        public static final int fake = -1;

    }


    public Achievement(Context context,String name,String description, int icon,float val,int type) {
        mDescription = description;
        mIconId = icon;
        mName = name;
        mValue = val;
        mType = type;
        checkIsCompleted(context);

    }

    public float getValue() {
        return mValue;
    }

    public void setValue(float value) {
        mValue = value;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getIcon() {
        return mIconId;
    }

    public void setIcon(int icon) {
        mIconId = icon;
    }

    public boolean isGot() {
        return mIsGot;
    }

    public void setGot(boolean got) {
        mIsGot = got;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public void checkIsCompleted(Context context){
        switch (mType){
            case AchievementType.distance:
                mIsGot = Player.instance(context).getDistance()>=mValue;
                break;
            case AchievementType.level:
                mIsGot = Player.instance(context).getLevel()>=mValue;
                break;
            case AchievementType.maxDistance:
                mIsGot = Player.instance(context).getMaxDistance()>=mValue;
                break;
            case AchievementType.maxSpeed:
                mIsGot = Player.instance(context).getMaxSpeed()>=mValue;
                break;
            case AchievementType.calories:
                mIsGot = Player.instance(context).getCalories()>=mValue;
                break;
            case AchievementType.fake:
                mIsGot = true;
                break;
        }

    }
}
