package com.highscores.runstory.Model.MapPoint;

import android.content.Context;

import com.highscores.runstory.Model.Player.Player;
import com.highscores.runstory.R;

/**
 * Created by михаил on 04.05.2017.
 */

public class MoneyPoint extends BasePoint {


    private static final double RADIUS = 75;


    private int mProfit;


    public MoneyPoint(int profit,Context context) {
        super(R.drawable.money_marker, RADIUS,context);
        mProfit = profit;
        mMessage = mProfit + " Монет";
        mCircleColor = mContext.getResources().getColor(R.color.moneysCircle);
        mCircleStrokeColor = mContext.getResources().getColor(R.color.moneysCircleStroke);
    }

    public int getProfit() {
        return mProfit;
    }

    public void setProfit(int profit) {
        mProfit = profit;
    }

    @Override
    public void completePoint() {
            super.completePoint();
            Player.instance(mContext).increaseCoinsCount(mProfit);
    }
}
