package com.highscores.runstory.Model.Task;

import android.content.Context;
import android.util.Log;

import com.highscores.runstory.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by михаил on 16.02.2017.
 */

public class TasksList {

    static List<Task> mTasks;
    private static Context mTasksContext;

    private static void fillTasks(){
        mTasks = new ArrayList<>();
        mTasks.add(new RunToPointTask(mTasksContext));
        mTasks.add(new RunTimeTask(mTasksContext));
    }

    public static Task getTask(int i,Context context){
        mTasksContext = context;
        if(mTasks == null){
            fillTasks();
        }
        return mTasks.get(i);
    }

    public static List<Task> getLastTacks(Context context){
        mTasksContext = context;
        if(mTasks == null) {
            fillTasks();
        }

        return mTasks;
    }

    public static Context getTasksContext() {
        return mTasksContext;
    }
}
