package com.highscores.runstory.Model.Player;

/**
 * Created by михаил on 14.03.2017.
 */

public class LevelUpParameters {

    static private float [] mDistance;

    static private final int levelsCount = 5000;

    public static float getDistanceToCompleteLevel(int num){
        if(num >levelsCount - 1){
            num = levelsCount;
        }
        createDistance();
        return mDistance[num];
    }

    static private void createDistance(){
        mDistance = new float[levelsCount];
        mDistance[0] = 0;
        mDistance[1] = 1;
        mDistance[2] = 2;
        mDistance[3] = 5;
        mDistance[4] = 10;
        for (int i = 5;i<mDistance.length;++i){
            mDistance[i] = mDistance[i-1] + 10;
        }
    }
}
