package com.highscores.runstory.Model.GameParameters;

import com.google.android.gms.maps.model.LatLng;
import com.highscores.runstory.Utility.Constance;
import com.highscores.runstory.Utility.TimeFormater;
import com.highscores.runstory.Utility.ValueConverter;
import com.jjoe64.graphview.series.DataPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by михаил on 09.05.2017.
 */

public class GameParameters {

    public GameParameters(){
        speedsList = new ArrayList<>();
        mPoints = new ArrayList<>();
    }



    private double distance;
    private float speed;

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    private float maxSpeed;
    private int time;
    private double calories;
    private int coins;

    public LatLng currentPosition;
    public List<DataPoint> speedsList;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
        if(speed>maxSpeed){
            maxSpeed = speed;
        }
    }

      public float getMaxSpeed() {
            return maxSpeed;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getCalories() {
        return calories;
    }

    public float getRode(){
        return ValueConverter.speedToRode(speed);
    }

    public float getMiddleSpeed(){
        return (float)distance/time * 3600;
    }

    public float getMiddleRode(){
        return ValueConverter.speedToRode(getMiddleSpeed());
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public int getCoins(){
        return coins + (int)(distance* Constance.MONEYS_PER_KM);
    }

    public void increaseCoins(int how){
        coins+=how;
    }

    public List<LatLng> getPoints() {
        return mPoints;
    }



    int mTotalCoins;

    public void setPoints(List<LatLng> points) {
        mPoints = points;
    }

    private List<LatLng> mPoints;


    public DataPoint[] getSpeeds() {
        DataPoint[] rez = new DataPoint[speedsList.size()];
        speedsList.toArray(rez);
        return rez;
    }

    public Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        map.put(GameParametersTypes.time, TimeFormater.formatTime(getTime()));
        map.put(GameParametersTypes.distance,String.format(String.format("%.2f",getDistance())));
        map.put(GameParametersTypes.speed,String.format(String.format("%.2f",getSpeed())));
        map.put(GameParametersTypes.calories, String.valueOf((int)getCalories()));
        map.put(GameParametersTypes.coins,String.valueOf(getCoins()));
        map.put(GameParametersTypes.rode,String.format(String.format("%.2f",getRode())));
        return map;
    }

}
