package com.highscores.runstory.Model.Player;

import android.content.Context;
import android.content.SharedPreferences;


public class Player {

    private int mLevel = 1;
    private float mDistance;
    private int mCoinsCount;
    private String mName;
    private int mGrown;
    private int mWidth;


    private float mMaxDistance;
    private float mMaxSpeed;


    private int mCalories;

    Context mContext;
    SharedPreferences mSaving;
    private static final String APP_PREFERENCES = "player";
    static Player mPlayer;

    private boolean firstTime;

    private Player(Context context){
        mContext = context;
        read();
    }

    public static Player instance(Context context){
        if(mPlayer == null){
            mPlayer = new Player(context);
        }
        return mPlayer;
    }

    public int getLevel() {
        return mLevel;
    }

    public float getDistance() {
        return mDistance;
    }

    public void increaseDistance(float how) {
        checkMaxDistance(how);
        mDistance += how;
        checkLevel();
        save();
    }

    public int getCoinsCount() {
        return mCoinsCount;
    }

    public void increaseCoinsCount(int how) {
        mCoinsCount += how;
        save();
    }

    public float getMaxDistance() {
        return mMaxDistance;
    }

    private void checkMaxDistance(float maxDistance) {
        if(maxDistance>mMaxDistance){
            mMaxDistance = maxDistance;
            save();
        }

    }

    public float getMaxSpeed() {
        return mMaxSpeed;
    }

    public void checkMaxSpeed(float maxSpeed) {
        if(maxSpeed>mMaxSpeed){
            mMaxSpeed = maxSpeed;
            save();
        }

    }

    private void save() {
        mSaving =  mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSaving.edit();
        editor.putInt("level",mLevel);
        editor.putFloat("dis",mDistance);
        editor.putInt("coins",mCoinsCount);
        editor.putInt("calories",mCalories);
        editor.putString("name",mName);
        editor.putInt("grown",mGrown);
        editor.putInt("width",mWidth);
        editor.putBoolean("first",false);
        editor.putFloat("ms",mMaxSpeed);
        editor.putFloat("md",mMaxDistance);
        editor.apply();
    }

    private void read(){
        mSaving =  mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        mLevel = mSaving.getInt("level",1);
        mDistance = mSaving.getFloat("dis",0);
        mCoinsCount = mSaving.getInt("coins",0);
        mCalories = mSaving.getInt("calories",0);
        mWidth = mSaving.getInt("width",0);
        mGrown = mSaving.getInt("grown",0);
        mName = mSaving.getString("name","Joe");
        mMaxDistance = mSaving.getFloat("md",0);
        mMaxSpeed = mSaving.getFloat("ms",mMaxSpeed);
        firstTime = mSaving.getBoolean("first",true);
    }

    private void checkLevel(){
        if(mDistance>=LevelUpParameters.getDistanceToCompleteLevel(mLevel)){
            ++mLevel;
            checkLevel();
        }
    }
    public int getCalories() {
        return mCalories;
    }

    public void increaseCalories(int how) {
        mCalories += how;
        save();
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
        save();
    }

    public int getGrown() {
        return mGrown;

    }

    public void setGrown(int grown) {
        mGrown = grown;
        save();
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
        save();
    }

    public boolean isFirstTime() {
       return firstTime;
    }

    public void isNotFirstTime(){
        firstTime = false;
        save();
    }
}
