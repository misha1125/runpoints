package com.highscores.runstory.Model.MapPoint;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.highscores.runstory.R;

/**
 * Created by михаил on 14.05.2017.
 */

public class FinishPoint extends BasePoint {

    static final int finishIcon = R.drawable.finish_marker;
    static final int radius = 50;

    private Polyline mPolyne;

    public boolean isActive() {
        return isActive;
    }

    boolean isActive = true;

    public FinishPoint(Context context) {
        super(finishIcon, radius, context);
        mCircleColor = context.getResources().getColor(R.color.finishCircle);
        mCircleStrokeColor = context.getResources().getColor(R.color.finishCircleStroke);
    }

    @Override
    protected void deletePoint() {
        super.deletePoint();
        mPolyne.remove();
        isActive = false;
    }

    @Override
    public void stand(GoogleMap map) {
        if(isActive){
            super.stand(map);
            if(mPolyne!=null){
                Polyline line =  map.addPolyline(new PolylineOptions().color(mPolyne.getColor()));
                line.setPoints(mPolyne.getPoints());
            }

        }
    }

    @Override
    public void standToMain(GoogleMap map) {
        super.standToMain(map);
        Polyline line =  map.addPolyline(new PolylineOptions().color(mPolyne.getColor()));
        line.setPoints(mPolyne.getPoints());
        mPolyne = line;
    }



    public void setPolyne(Polyline polyne) {
        mPolyne = polyne;
    }
}
