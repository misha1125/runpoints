package com.highscores.runstory.Model.MapPoint;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.highscores.runstory.Utility.Constance;

/**
 * Created by михаил on 04.05.2017.
 */

public class BasePoint {
    protected int mIconId;
    protected double mRadius;
    protected String mMessage = "";

    public int getCircleColor() {
        return mCircleColor;
    }

    public void setCircleColor(int circleColor) {
        mCircleColor = circleColor;
    }

    public int getCircleStrokeColor() {
        return mCircleStrokeColor;
    }

    public void setCircleStrokeColor(int circleStrokeColor) {
        mCircleStrokeColor = circleStrokeColor;
    }

    protected int mCircleColor;
    protected int mCircleStrokeColor;
    protected Context mContext;

    boolean mWasInCollectedState;

    public LatLng getPosition() {
        return mPosition;
    }

    public void setPosition(LatLng position) {
        mPosition = position;
    }

    protected LatLng mPosition;


    protected Marker mMarker;

    protected boolean mActive = true;

    public Circle getCircle() {
        return mCircle;
    }

    public void setCircle(Circle circle) {
        mCircle = circle;
    }



    protected Circle mCircle;

    public BasePoint(int iconId, double radius,Context context) {
        mIconId = iconId;
        mRadius = radius;
        mContext = context;
    }

    public int getIconId() {
        return mIconId;
    }

    public void setIconId(int iconId) {
        mIconId = iconId;
    }

    public double getRadius() {
        return mRadius;
    }

    public void setRadius(double radius) {
        mRadius = radius;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }


    public Marker getMarker() {
        return mMarker;
    }

    public void setMarker(Marker marker) {
        mMarker = marker;
    }

    void completePoint(){
        deletePoint();
    }

    public boolean checkPoint(LatLng currentLocation){
        if(mActive){
            if(Constance.distance(currentLocation,mPosition)<=mRadius){
                    completePoint();
                    mActive = false;
                    return true;
            }
        }
       return false;
    }

    public void stand(GoogleMap map){
        map.addMarker(new MarkerOptions()
                .icon(Constance.bitmapScale(mContext,getIconId(),3))
                .title(getMessage())
                .position(getPosition()));

        map.addCircle(new CircleOptions()
                .center(getPosition())
                .fillColor(mCircleColor)
                .strokeColor(mCircleStrokeColor)
                .strokeWidth(2f)
                .radius(getRadius()));
    }

    public void standToMain(GoogleMap map){
        Marker marker = map.addMarker(new MarkerOptions()
                .icon(Constance.bitmapScale(mContext,getIconId(),3))
                .title(getMessage())
                .position(getPosition()));
        setMarker(marker);
        setCircle(map.addCircle(new CircleOptions()
                .center(getPosition())
                .fillColor(mCircleColor)
                .strokeColor(mCircleStrokeColor)
                .strokeWidth(2f)
                .radius(getRadius())));
    }

    protected void deletePoint(){
        mMarker.remove();
        mCircle.remove();
    }
}
