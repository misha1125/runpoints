package com.highscores.runstory.Model.Task;

import android.content.Context;
import android.content.SharedPreferences;

import com.highscores.runstory.Controller.Task.TaskController;

import java.util.Arrays;
import java.util.List;

/**
 * Created by михаил on 16.02.2017.
 */

public abstract class Task {
    public int getCost() {
        return mCost;
    }

    public TaskController getTaskController() {
        return mTaskController;
    }

    TaskController mTaskController;


    static final String APP_PREFERENCES = "tasks";
    public static final String TASK_NUM = "taskNum";

    private SharedPreferences mSettings;

    Context mContext;

    public boolean isBought() {
        return isBought;
    }

    boolean isBought;

    private String mTitle;

    private String mDescription;

    private int mLastValueIndex;
    private List<Integer> mMaybeValues;

    int id;

    static int currentId;

    private int mCost;

    public int getPrise() {
        return mPrise;
    }

    private int mPrise;

    Task(String title, String description,Integer[] maybeValues,int cost,int coins,Context context) {

        mTitle = title;
        mDescription = description;
        mMaybeValues = Arrays.asList(maybeValues);
        mContext = context;
        id = currentId++;
        mCost = cost;
        mPrise = coins;
        readCurrentIndex();
        read();
    }


    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getCurrentValue(){
        return mMaybeValues.get(mLastValueIndex);
    }

    public int currentProgress(){
       return mLastValueIndex;
    }

    public int tasksCount(){
       return mMaybeValues.size();
    }

   public String getProgressString(){
        return (mLastValueIndex+1) + "/" + mMaybeValues.size();
    }

    public void nextTask(){
        if(mLastValueIndex<mMaybeValues.size()){
            ++mLastValueIndex;
            saveCurrentIndex();
        }
    }

    private void readCurrentIndex(){
        mSettings =  mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        mLastValueIndex = mSettings.getInt("task" +id ,0);
    }

    private void saveCurrentIndex(){
        mSettings =  mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt("task" +id,mLastValueIndex);
        editor.apply();
    }




    public void buy(){
        isBought = true;
        save();
    }

    void read(){
       mSettings =  mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
       isBought = mSettings.getBoolean("buy" +id ,false);
    }

    void save(){
        mSettings =  mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean("buy" +id,isBought);
        editor.apply();
    }
}
