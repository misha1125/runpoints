package com.highscores.runstory.Model.Achievements;

import android.content.Context;

import com.highscores.runstory.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by михаил on 06.06.2017.
 */

public class AchievementsList {

    static List<Achievement> mAchievements;

    private static void fillAchievements(Context context){
        mAchievements = new ArrayList<>();

        mAchievements.add(new Achievement(context,context.getString(R.string.firstStep),context.getString(R.string.first_task), R.drawable.cake_start,1, Achievement.AchievementType.fake));

        mAchievements.add(new Achievement(context,context.getString(R.string.short_running),context.getString(R.string.run_one), R.drawable.distance1,1, Achievement.AchievementType.distance));
        mAchievements.add(new Achievement(context,context.getString(R.string.first_training),context.getString(R.string.run_one_traning), R.drawable.runner1,
                1, Achievement.AchievementType.maxDistance));
        mAchievements.add(new Achievement(context,context.getString(R.string.wserious_distance),context.getString(R.string.run_ten),R.drawable.distance2,10,Achievement.AchievementType.distance));
        mAchievements.add(new Achievement(context,context.getString(R.string.come_to_fit),context.getString(R.string.run_two_ono_raining), R.drawable.max_distance1,
                1, Achievement.AchievementType.maxDistance));
        mAchievements.add(new Achievement(context,context.getString(R.string.acelerate),context.getString(R.string.acel_12), R.drawable.top_speed1,
                12, Achievement.AchievementType.maxSpeed));
        mAchievements.add(new Achievement(context,context.getString(R.string.hamburger),context.getString(R.string.hamburger_calor), R.drawable.burger,
                295, Achievement.AchievementType.calories));
        mAchievements.add(new Achievement(context,context.getString(R.string.long_distancec),context.getString(R.string.run_5_for_one), R.drawable.max_distance3,
                5, Achievement.AchievementType.maxDistance));
        mAchievements.add(new Achievement(context,context.getString(R.string.faster_then_rain),context.getString(R.string.acel_15), R.drawable.top_speed2,
                15, Achievement.AchievementType.maxSpeed));

        mAchievements.add(new Achievement(context,context.getString(R.string.atlet),context.getString(R.string.run_ten_for_one), R.drawable.max_distance5,
                10, Achievement.AchievementType.maxDistance));

        mAchievements.add(new Achievement(context,context.getString(R.string.halfmarafon),context.getString(R.string.run_20),R.drawable.half_marafon,20,Achievement.AchievementType.distance));

        mAchievements.add(new Achievement(context,context.getString(R.string.marafoner),context.getString(R.string.run_20_one), R.drawable.goal2,
                20, Achievement.AchievementType.maxDistance));

        mAchievements.add(new Achievement(context,context.getString(R.string.cake),context.getString(R.string.bern_1k), R.drawable.cake,
                1000, Achievement.AchievementType.calories));
        mAchievements.add(new Achievement(context,context.getString(R.string.sprinter),context.getString(R.string.acel_20), R.drawable.top_speed3,
                20, Achievement.AchievementType.maxSpeed));
        mAchievements.add(new Achievement(context,context.getString(R.string.marafon),context.getString(R.string.run_40),R.drawable.flag,40,Achievement.AchievementType.distance));

        mAchievements.add(new Achievement(context,context.getString(R.string.faster_then_bullet),context.getString(R.string.acel_25), R.drawable.top_speed4,
                25, Achievement.AchievementType.maxSpeed));
        mAchievements.add(new Achievement(context,context.getString(R.string.ten_chocolates),context.getString(R.string.bern_5k), R.drawable.chocolate,
                5000, Achievement.AchievementType.calories));
        mAchievements.add(new Achievement(context,context.getString(R.string.one_hungred),context.getString(R.string.run_100),R.drawable.goal,100,Achievement.AchievementType.distance));
    }

    public static List<Achievement> getAchievementsList(Context context){
        fillAchievements(context);
        return mAchievements;
    }
}
