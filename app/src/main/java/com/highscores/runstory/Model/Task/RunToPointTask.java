package com.highscores.runstory.Model.Task;

import android.content.Context;

import com.highscores.runstory.Controller.Task.RunToPointController;
import com.highscores.runstory.R;

/**
 * Created by михаил on 07.08.2017.
 */

public class RunToPointTask extends Task {



    RunToPointTask(Context context) {
        super(context.getString(R.string.distance_title),  context.getString(R.string.distance_description),
                new Integer[]{500,1000,1200,1500,2000,2500,3000,4000,5000,7000,10000,12000},50,0, context);
        mTaskController = new RunToPointController(context,this);
    }


}
