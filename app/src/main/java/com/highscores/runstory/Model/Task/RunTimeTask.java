package com.highscores.runstory.Model.Task;

import android.content.Context;

import com.highscores.runstory.Controller.Task.RunTimeController;
import com.highscores.runstory.R;

/**
 * Created by михаил on 17.08.2017.
 */

public class RunTimeTask extends Task {
    RunTimeTask(Context context) {
        super( context.getString(R.string.time_title),context.getString(R.string.time_description),
                new Integer[]{120,5*60,10*60,12*60,15*60,20*60,25*60,30*60,40*60,50*60,60*60,65*60},
                30, 0, context);
        mTaskController = new RunTimeController(context,this);
    }
}
