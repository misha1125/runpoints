package com.highscores.runstory.Model.GameParameters;


/**
 * Created by михаил on 03.09.2017.
 */

public final class GameParametersTypes {
    public final static String time = "time";
    public final static String speed = "speed";
    public final static String distance = "distance";
    public final static String coins = "coins";
    public final static String calories = "calories";
    public final static String rode = "rode";


}
