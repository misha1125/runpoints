package com.highscores.runstory.data;

import android.provider.BaseColumns;

/**
 * Created by михаил on 26.06.2017.
 */

public final class TrainingContract {
    private TrainingContract() {
    };

    public static final class TrainingEntry implements BaseColumns {
        public final static String TABLE_NAME = "training";

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_DATE = "date";
        public final static String COLUMN_DISTANCE = "distance";
        public final static String COLUMN_CALORIES = "calories";
        public final static String COLUMN_TIME = "time";
        public final static String COLUMN_SPEED = "speed";
        public final static String COLUMN_POINTS = "points";
        public final static String COLUMN_MAX_SPEED = "maxSpeed";
        public final static String COLUMN_MONEYS = "moneys";
    }
}
