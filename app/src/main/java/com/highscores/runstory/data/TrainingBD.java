package com.highscores.runstory.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.highscores.runstory.Model.Training.Training;

/**
 * Created by михаил on 26.06.2017.
 */

public class TrainingBD extends SQLiteOpenHelper {
    public static final String LOG_TAG = TrainingBD.class.getSimpleName();

    /**
     * Имя файла базы данных
     */
    private static final String DATABASE_NAME = "trainings.db";

    /**
     * Версия базы данных. При изменении схемы увеличить на единицу
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Конструктор {@link TrainingBD}.
     *
     * @param context Контекст приложения
     */
    public TrainingBD(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Вызывается при создании базы данных
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Строка для создания таблицы
        String SQL_CREATE_GUESTS_TABLE = "CREATE TABLE " + TrainingContract.TrainingEntry.TABLE_NAME + " ("
                + TrainingContract.TrainingEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + TrainingContract.TrainingEntry.COLUMN_DATE + " INTEGER, "
                + TrainingContract.TrainingEntry.COLUMN_CALORIES + " INTEGER, "
                + TrainingContract.TrainingEntry.COLUMN_DISTANCE + " REAL, "
                + TrainingContract.TrainingEntry.COLUMN_MAX_SPEED + " REAL, "
                + TrainingContract.TrainingEntry.COLUMN_TIME + " INTEGER, "
                + TrainingContract.TrainingEntry.COLUMN_MONEYS + " INTEGER, "
                + TrainingContract.TrainingEntry.COLUMN_POINTS + " STRING, "
                + TrainingContract.TrainingEntry.COLUMN_SPEED + " STRING);";

        // Запускаем создание таблицы
        db.execSQL(SQL_CREATE_GUESTS_TABLE);
    }

    /**
     * Вызывается при обновлении схемы базы данных
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
