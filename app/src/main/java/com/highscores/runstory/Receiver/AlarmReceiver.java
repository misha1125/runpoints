package com.highscores.runstory.Receiver;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.highscores.runstory.Activities.MainMenu.Fragments.Schedule.ScheduleFragment;
import com.highscores.runstory.Activities.MainMenu.MainActivity;
import com.highscores.runstory.R;
import com.highscores.runstory.Utility.TimeFormater;

import java.util.Calendar;

/**
 * Created by михаил on 23.06.2017.
 */

public class AlarmReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        repeat(context);
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if(ScheduleFragment.readWeek(context)[ScheduleFragment.indexFromDay(day)]){
            showNotification(context);
        }
    }

    public static void repeat(Context context){
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.set(Calendar.HOUR_OF_DAY, ScheduleFragment.readTime(context)[1]);
        calendar.set(Calendar.MINUTE,ScheduleFragment.readTime(context)[0]);
        calendar.set(Calendar.SECOND,0);
        long timeToStart = calendar.getTimeInMillis();
        if(System.currentTimeMillis()>timeToStart){
            timeToStart+= 24 * 3600 * 1000;
        }

        Log.d("timeStart", TimeFormater.formatDate(timeToStart) + TimeFormater.formatTime((int)((timeToStart%(24 * 60 * 60 * 1000))/1000)) + "res");
        alarmManager.set(AlarmManager.RTC_WAKEUP, timeToStart, alarmIntent);
    }

    private void showNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent= PendingIntent.getActivity(context,0,intent1,0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.aplication_logo_not);
        builder.setContentTitle("Напоминание от RunPoints");
        builder.setContentText("Пора заниматься");
        builder.setPriority(Notification.PRIORITY_MAX);
        builder.setDefaults(Notification.DEFAULT_SOUND);
        builder.setLights(0x0000FF,3000,2000);
        builder.setContentIntent(pendingIntent);
        notificationManager.notify(56, builder.build());
    }
}